/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/main/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'hand2',
                            type: 'image',
                            rect: ['479px', '1088px', '960px', '866px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"hand2.png",'0px','0px']
                        },
                        {
                            id: 'light_sym',
                            symbolName: 'light_sym',
                            type: 'rect',
                            rect: ['413', '-6', '1093', '1092', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'heart',
                            display: 'none',
                            type: 'image',
                            rect: ['592px', '280px', '738px', '605px', 'auto', 'auto'],
                            opacity: '0.50406504065041',
                            fill: ["rgba(0,0,0,0)",im+"heart.png",'0px','0px'],
                            transform: [[],[],[],['0.5','0.5']]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(163,162,162,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 15000,
                    autoPlay: true,
                    data: [
                        [
                            "eid22",
                            "opacity",
                            250,
                            750,
                            "easeOutBack",
                            "${heart}",
                            '0.5040649771690369',
                            '1'
                        ],
                        [
                            "eid28",
                            "opacity",
                            0,
                            1500,
                            "linear",
                            "${light_sym}",
                            '0',
                            '1'
                        ],
                        [
                            "eid25",
                            "display",
                            0,
                            0,
                            "easeOutCubic",
                            "${heart}",
                            'none',
                            'none'
                        ],
                        [
                            "eid26",
                            "display",
                            250,
                            0,
                            "easeOutCubic",
                            "${heart}",
                            'none',
                            'block'
                        ],
                        [
                            "eid24",
                            "scaleY",
                            250,
                            750,
                            "easeOutBack",
                            "${heart}",
                            '0.5',
                            '1'
                        ],
                        [
                            "eid23",
                            "scaleX",
                            250,
                            750,
                            "easeOutBack",
                            "${heart}",
                            '0.5',
                            '1'
                        ],
                        [
                            "eid18",
                            "top",
                            500,
                            500,
                            "easeOutCubic",
                            "${hand2}",
                            '1088px',
                            '215px'
                        ]
                    ]
                }
            },
            "light_sym": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '1093px', '1092px', 'auto', 'auto'],
                            id: 'light',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', im + 'light.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '1093px', '1092px']
                        }
                    }
                },
                timeline: {
                    duration: 15000,
                    autoPlay: true,
                    data: [
                        [
                            "eid29",
                            "rotateZ",
                            0,
                            15000,
                            "linear",
                            "${light}",
                            '0deg',
                            '360deg'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_0");
