/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/10/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'question2img1Copy',
                            type: 'image',
                            rect: ['767px', '727px', '1156px', '776px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "3-10.png",'0px','0px']
                        },
                        {
                            id: 'question2img1',
                            type: 'image',
                            rect: ['665px', '727px', '897px', '542px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "2-10.png",'0px','0px']
                        },
                        {
                            id: 'question2img22',
                            type: 'image',
                            rect: ['706px', '608px', '1192px', '484px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "1-10.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 1750,
                    autoPlay: true,
                    data: [
                        [
                            "eid16",
                            "top",
                            750,
                            500,
                            "easeOutBack",
                            "${question2img1}",
                            '1440px',
                            '550px'
                        ],
                        [
                            "eid17",
                            "top",
                            250,
                            500,
                            "easeOutBack",
                            "${question2img22}",
                            '1139px',
                            '608px'
                        ],
                        [
                            "eid18",
                            "top",
                            1250,
                            500,
                            "easeOutBack",
                            "${question2img1Copy}",
                            '1440px',
                            '373px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_10");
