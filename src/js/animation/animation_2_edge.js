/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'question2img22',
                            type: 'image',
                            rect: ['1248px', '426px', '248px', '654px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"question2img22.png",'0px','0px']
                        },
                        {
                            id: 'question2img1',
                            type: 'image',
                            rect: ['1049px', '727px', '646px', '353px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"question2img1.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 1500,
                    autoPlay: true,
                    data: [
                        [
                            "eid17",
                            "top",
                            1000,
                            500,
                            "easeOutBack",
                            "${question2img22}",
                            '1139px',
                            '426px'
                        ],
                        [
                            "eid16",
                            "top",
                            250,
                            500,
                            "easeOutBack",
                            "${question2img1}",
                            '1440px',
                            '727px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_2");
