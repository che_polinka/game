/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/9/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Symbol_2',
                            symbolName: 'Symbol_2',
                            type: 'rect',
                            rect: ['1130', '352', '1482', '516', 'auto', 'auto']
                        },
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            type: 'rect',
                            rect: ['922', '270', '389', '647', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_1-9',
                            type: 'image',
                            rect: ['0px', '0px', '389px', '647px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '1-9.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '389px', '647px']
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [
                        [
                            "eid54",
                            "left",
                            0,
                            3000,
                            "easeInOutQuad",
                            "${_1-9}",
                            '0px',
                            '-18px'
                        ],
                        [
                            "eid64",
                            "left",
                            3000,
                            3000,
                            "easeInOutQuad",
                            "${_1-9}",
                            '-18px',
                            '0px'
                        ],
                        [
                            "eid55",
                            "top",
                            0,
                            3000,
                            "easeInOutQuad",
                            "${_1-9}",
                            '0px',
                            '34px'
                        ],
                        [
                            "eid65",
                            "top",
                            3000,
                            3000,
                            "easeInOutQuad",
                            "${_1-9}",
                            '34px',
                            '0px'
                        ]
                    ]
                }
            },
            "Symbol_2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_2-9',
                            type: 'image',
                            rect: ['0px', '0px', '1482px', '516px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '2-9.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '1482px', '516px']
                        }
                    }
                },
                timeline: {
                    duration: 10000,
                    autoPlay: true,
                    data: [
                        [
                            "eid67",
                            "top",
                            0,
                            5000,
                            "easeInOutQuad",
                            "${_2-9}",
                            '0px',
                            '6px'
                        ],
                        [
                            "eid74",
                            "top",
                            5000,
                            5000,
                            "easeInOutQuad",
                            "${_2-9}",
                            '6px',
                            '0px'
                        ],
                        [
                            "eid66",
                            "left",
                            0,
                            5000,
                            "easeInOutQuad",
                            "${_2-9}",
                            '0px',
                            '-28px'
                        ],
                        [
                            "eid73",
                            "left",
                            5000,
                            5000,
                            "easeInOutQuad",
                            "${_2-9}",
                            '-28px',
                            '0px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_9");
