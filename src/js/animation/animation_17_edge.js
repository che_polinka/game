/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/17/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Symbol_2',
                            symbolName: 'Symbol_2',
                            type: 'rect',
                            rect: ['882', '520px', '391', '607', 'auto', 'auto']
                        },
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            type: 'rect',
                            rect: ['1366', '566px', '386', '561', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 1250,
                    autoPlay: true,
                    data: [
                        [
                            "eid23",
                            "top",
                            750,
                            500,
                            "easeOutBack",
                            "${Symbol_2}",
                            '1130px',
                            '520px'
                        ],
                        [
                            "eid22",
                            "top",
                            250,
                            500,
                            "easeOutBack",
                            "${Symbol_1}",
                            '1176px',
                            '566px'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_17-1',
                            type: 'image',
                            rect: ['0px', '0px', '386px', '561px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '17-1.png', '0px', '0px']
                        },
                        {
                            rect: ['44px', '108px', '60px', '56px', 'auto', 'auto'],
                            id: 'Rectangle2',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(255,186,153,1.00)']
                        },
                        {
                            rect: ['168px', '120px', '68px', '44px', 'auto', 'auto'],
                            id: 'Rectangle',
                            stroke: [0, 'rgba(0,0,0,1)', 'none'],
                            type: 'rect',
                            fill: ['rgba(255,186,153,1.00)']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '386px', '561px']
                        }
                    }
                },
                timeline: {
                    duration: 5000,
                    autoPlay: true,
                    data: [
                        [
                            "eid49",
                            "display",
                            3500,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'none',
                            'none'
                        ],
                        [
                            "eid50",
                            "display",
                            3815,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid51",
                            "display",
                            4065,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid54",
                            "display",
                            3500,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'none',
                            'none'
                        ],
                        [
                            "eid55",
                            "display",
                            3815,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'none',
                            'block'
                        ],
                        [
                            "eid56",
                            "display",
                            4065,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'block',
                            'none'
                        ]
                    ]
                }
            },
            "Symbol_2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_17-2',
                            type: 'image',
                            rect: ['0px', '0px', '391px', '607px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '17-2.png', '0px', '0px']
                        },
                        {
                            rect: ['148px', '158px', '74px', '62px', 'auto', 'auto'],
                            id: 'Rectangle',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(255,186,153,1)']
                        },
                        {
                            rect: ['284px', '172px', '54px', '38px', 'auto', 'auto'],
                            id: 'Rectangle2',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(255,186,153,1)']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '391px', '607px']
                        }
                    }
                },
                timeline: {
                    duration: 5000,
                    autoPlay: true,
                    data: [
                        [
                            "eid44",
                            "display",
                            1500,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'none',
                            'none'
                        ],
                        [
                            "eid45",
                            "display",
                            1815,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid46",
                            "display",
                            2065,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid47",
                            "display",
                            2500,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid48",
                            "display",
                            2750,
                            0,
                            "easeOutBack",
                            "${Rectangle2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid39",
                            "display",
                            1500,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'none',
                            'none'
                        ],
                        [
                            "eid40",
                            "display",
                            1815,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'none',
                            'block'
                        ],
                        [
                            "eid41",
                            "display",
                            2065,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'block',
                            'none'
                        ],
                        [
                            "eid42",
                            "display",
                            2500,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'none',
                            'block'
                        ],
                        [
                            "eid43",
                            "display",
                            2750,
                            0,
                            "easeOutBack",
                            "${Rectangle}",
                            'block',
                            'none'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_17");
