/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/3/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Symbol_2',
                            symbolName: 'Symbol_2',
                            type: 'rect',
                            rect: ['1144px', '374', '291', '706', 'auto', 'auto']
                        },
                        {
                            id: 'question3img2',
                            type: 'image',
                            rect: ['0px', '1164px', '1920px', '244px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"question3img2.png",'0px','0px']
                        },
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            display: 'none',
                            type: 'rect',
                            rect: ['1142px', '946px', '54', '56', 'auto', 'auto'],
                            transform: [[],['305']]
                        },
                        {
                            id: 'question3img332',
                            display: 'block',
                            type: 'image',
                            rect: ['1142px', '946px', '54px', '56px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"question3img33.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 11000,
                    autoPlay: true,
                    data: [
                        [
                            "eid70",
                            "scaleX",
                            8000,
                            600,
                            "linear",
                            "${Symbol_1}",
                            '1',
                            '1.3'
                        ],
                        [
                            "eid72",
                            "scaleX",
                            8600,
                            800,
                            "linear",
                            "${Symbol_1}",
                            '1.3',
                            '1'
                        ],
                        [
                            "eid74",
                            "scaleX",
                            10000,
                            600,
                            "linear",
                            "${Symbol_1}",
                            '1',
                            '1.2'
                        ],
                        [
                            "eid61",
                            "display",
                            0,
                            0,
                            "easeOutCirc",
                            "${Symbol_1}",
                            'none',
                            'none'
                        ],
                        [
                            "eid62",
                            "display",
                            8000,
                            0,
                            "easeOutCirc",
                            "${Symbol_1}",
                            'none',
                            'block'
                        ],
                        [
                            "eid60",
                            "display",
                            8000,
                            0,
                            "easeOutCirc",
                            "${question3img332}",
                            'block',
                            'none'
                        ],
                        [
                            "eid71",
                            "scaleY",
                            8000,
                            600,
                            "linear",
                            "${Symbol_1}",
                            '1',
                            '1.3'
                        ],
                        [
                            "eid73",
                            "scaleY",
                            8600,
                            800,
                            "linear",
                            "${Symbol_1}",
                            '1.3',
                            '1'
                        ],
                        [
                            "eid75",
                            "scaleY",
                            10000,
                            600,
                            "linear",
                            "${Symbol_1}",
                            '1',
                            '1.2'
                        ],
                        [
                            "eid81",
                            "top",
                            500,
                            500,
                            "easeOutCirc",
                            "${question3img332}",
                            '1274px',
                            '946px'
                        ],
                        [
                            "eid64",
                            "rotateZ",
                            8000,
                            600,
                            "linear",
                            "${Symbol_1}",
                            '0deg',
                            '90deg'
                        ],
                        [
                            "eid65",
                            "rotateZ",
                            8600,
                            400,
                            "linear",
                            "${Symbol_1}",
                            '90deg',
                            '200deg'
                        ],
                        [
                            "eid66",
                            "rotateZ",
                            9000,
                            400,
                            "linear",
                            "${Symbol_1}",
                            '200deg',
                            '305deg'
                        ],
                        [
                            "eid67",
                            "rotateZ",
                            9400,
                            600,
                            "linear",
                            "${Symbol_1}",
                            '305deg',
                            '336deg'
                        ],
                        [
                            "eid68",
                            "rotateZ",
                            10000,
                            400,
                            "linear",
                            "${Symbol_1}",
                            '336deg',
                            '369deg'
                        ],
                        [
                            "eid69",
                            "rotateZ",
                            10400,
                            400,
                            "linear",
                            "${Symbol_1}",
                            '369deg',
                            '341deg'
                        ],
                        [
                            "eid34",
                            "top",
                            500,
                            500,
                            "easeOutCirc",
                            "${question3img2}",
                            '1164px',
                            '836px'
                        ],
                        [
                            "eid52",
                            "left",
                            750,
                            8250,
                            "linear",
                            "${Symbol_2}",
                            '1949px',
                            '1094px'
                        ],
                        [
                            "eid63",
                            "location",
                            8000,
                            3000,
                            "linear",
                            "${Symbol_1}",
                            [[1169, 968, 0, 0, 0, 0,0],[1156.63, 965.67, -436.57, -91.49, -28.1, -5.89,12.59],[1021.8, 478.67, 662.68, -201.6, 899.72, -273.71,679.15],[1105.42, 816.65, -176.76, 71.82, -601, 244.2,1216.21],[514.09, 837.51, -761.05, -312.92, -677.88, -278.72,1819.61],[36.22, 531.57, -403.23, -283, -552.53, -387.79,2389.54],[-241, 334, 0, 0, 0, 0,2729.97]]
                        ],
                            [ "eid55", "trigger", 0, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${Symbol_2}', [] ] ],
                            [ "eid56", "trigger", 2500, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${Symbol_2}', [] ] ],
                            [ "eid57", "trigger", 5000, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${Symbol_2}', [] ] ],
                            [ "eid76", "trigger", 7500, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${Symbol_2}', [] ] ],
                            [ "eid77", "trigger", 9000, function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${Symbol_2}', [] ] ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '54px', '56px', 'auto', 'auto'],
                            type: 'image',
                            id: 'question3img33',
                            opacity: '1',
                            display: 'none',
                            fill: ['rgba(0,0,0,0)', im + 'question3img33.png', '0px', '0px']
                        },
                        {
                            rect: ['0px', '0px', '54px', '56px', 'auto', 'auto'],
                            type: 'image',
                            id: 'question3img32',
                            opacity: '1',
                            display: 'none',
                            fill: ['rgba(0,0,0,0)', im + 'question3img32.png', '0px', '0px']
                        },
                        {
                            rect: ['0px', '0px', '54px', '56px', 'auto', 'auto'],
                            type: 'image',
                            id: 'question3img31',
                            opacity: '1',
                            display: 'block',
                            fill: ['rgba(0,0,0,0)', im + 'question3img31.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '54px', '56px']
                        }
                    }
                },
                timeline: {
                    duration: 375,
                    autoPlay: true,
                    data: [
                        [
                            "eid22",
                            "display",
                            0,
                            0,
                            "easeOutBack",
                            "${question3img33}",
                            'none',
                            'block'
                        ],
                        [
                            "eid24",
                            "display",
                            125,
                            0,
                            "easeOutBack",
                            "${question3img33}",
                            'block',
                            'none'
                        ],
                        [
                            "eid18",
                            "display",
                            0,
                            0,
                            "easeOutBack",
                            "${question3img31}",
                            'block',
                            'none'
                        ],
                        [
                            "eid26",
                            "display",
                            250,
                            0,
                            "easeOutBack",
                            "${question3img31}",
                            'none',
                            'block'
                        ],
                        [
                            "eid27",
                            "display",
                            375,
                            0,
                            "easeOutBack",
                            "${question3img31}",
                            'block',
                            'none'
                        ],
                        [
                            "eid19",
                            "display",
                            0,
                            0,
                            "easeOutBack",
                            "${question3img32}",
                            'none',
                            'none'
                        ],
                        [
                            "eid20",
                            "display",
                            125,
                            0,
                            "easeOutBack",
                            "${question3img32}",
                            'none',
                            'block'
                        ],
                        [
                            "eid23",
                            "display",
                            250,
                            0,
                            "easeOutBack",
                            "${question3img32}",
                            'block',
                            'none'
                        ],
                        [
                            "eid30",
                            "display",
                            375,
                            0,
                            "easeOutBack",
                            "${question3img32}",
                            'none',
                            'block'
                        ]
                    ]
                }
            },
            "Symbol_2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'question3img1',
                            type: 'image',
                            rect: ['0px', '-10px', '291px', '706px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + 'question3img1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '291px', '706px']
                        }
                    }
                },
                timeline: {
                    duration: 2500,
                    autoPlay: true,
                    data: [
                        [
                            "eid37",
                            "top",
                            0,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '0px',
                            '-20px'
                        ],
                        [
                            "eid38",
                            "top",
                            250,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '-20px',
                            '0px'
                        ],
                        [
                            "eid39",
                            "top",
                            500,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '0px',
                            '-10px'
                        ],
                        [
                            "eid44",
                            "top",
                            750,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '-10px',
                            '0px'
                        ],
                        [
                            "eid45",
                            "top",
                            1500,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '0px',
                            '-20px'
                        ],
                        [
                            "eid46",
                            "top",
                            1750,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '-20px',
                            '0px'
                        ],
                        [
                            "eid47",
                            "top",
                            2000,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '0px',
                            '-10px'
                        ],
                        [
                            "eid48",
                            "top",
                            2250,
                            250,
                            "easeOutCirc",
                            "${question3img1}",
                            '-10px',
                            '0px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);
})("animation_3");
