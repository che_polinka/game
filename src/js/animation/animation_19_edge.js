/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/19/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_19-3',
                            type: 'image',
                            rect: ['1628px', '-721px', '239px', '679px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "19-3.png",'0px','0px']
                        },
                        {
                            id: '_19-2',
                            type: 'image',
                            rect: ['1267px', '-571px', '239px', '529px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "19-2.png",'0px','0px']
                        },
                        {
                            id: '_19-1',
                            type: 'image',
                            rect: ['884px', '-391px', '239px', '349px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "19-1.png",'0px','0px']
                        },
                        {
                            id: '_19-6',
                            type: 'image',
                            rect: ['1732px', '312px', '32px', '49px', 'auto', 'auto'],
                            clip: 'rect(-88px 32px 85px 0px)',
                            fill: ["rgba(0,0,0,0)",im + "19-6.png",'0px','0px']
                        },
                        {
                            id: '_19-5',
                            type: 'image',
                            rect: ['1247px', '-83px', '270px', '34px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "19-5.png",'0px','0px']
                        },
                        {
                            id: '_19-4',
                            type: 'image',
                            rect: ['920px', '-83px', '167px', '34px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "19-4.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 17500,
                    autoPlay: true,
                    data: [
                        [
                            "eid65",
                            "top",
                            2500,
                            750,
                            "easeOutCirc",
                            "${_19-6}",
                            '-794px',
                            '352px'
                        ],
                        [
                            "eid75",
                            "top",
                            4000,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '352px',
                            '282px'
                        ],
                        [
                            "eid76",
                            "top",
                            4500,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '282px',
                            '312px'
                        ],
                        [
                            "eid77",
                            "top",
                            6500,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '312px',
                            '282px'
                        ],
                        [
                            "eid78",
                            "top",
                            7000,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '282px',
                            '312px'
                        ],
                        [
                            "eid79",
                            "top",
                            9000,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '312px',
                            '282px'
                        ],
                        [
                            "eid80",
                            "top",
                            9500,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '282px',
                            '312px'
                        ],
                        [
                            "eid81",
                            "top",
                            11500,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '312px',
                            '282px'
                        ],
                        [
                            "eid82",
                            "top",
                            12000,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '282px',
                            '312px'
                        ],
                        [
                            "eid83",
                            "top",
                            14000,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '312px',
                            '282px'
                        ],
                        [
                            "eid84",
                            "top",
                            14500,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '282px',
                            '312px'
                        ],
                        [
                            "eid85",
                            "top",
                            16500,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '312px',
                            '282px'
                        ],
                        [
                            "eid86",
                            "top",
                            17000,
                            500,
                            "easeOutCirc",
                            "${_19-6}",
                            '282px',
                            '312px'
                        ],
                        [
                            "eid64",
                            "top",
                            1500,
                            750,
                            "easeOutBack",
                            "${_19-2}",
                            '-571px',
                            '551px'
                        ],
                        [
                            "eid60",
                            "top",
                            500,
                            750,
                            "easeOutBack",
                            "${_19-1}",
                            '-391px',
                            '731px'
                        ],
                        [
                            "eid62",
                            "top",
                            2500,
                            750,
                            "easeOutBack",
                            "${_19-3}",
                            '-721px',
                            '401px'
                        ],
                        [
                            "eid74",
                            "top",
                            500,
                            750,
                            "easeOutCirc",
                            "${_19-4}",
                            '-83px',
                            '697px'
                        ],
                        [
                            "eid71",
                            "top",
                            1500,
                            750,
                            "easeOutCirc",
                            "${_19-5}",
                            '-83px',
                            '517px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_19");
