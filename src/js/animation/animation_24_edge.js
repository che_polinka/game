/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/24/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_24-3',
                            type: 'image',
                            rect: ['1356px', '-25px', '6px', '926px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "24-3.png",'0px','0px'],
                            transform: [[],[],['-5']]
                        },
                        {
                            id: '_24-2',
                            type: 'image',
                            rect: ['826px', '-25px', '6px', '926px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "24-2.png",'0px','0px'],
                            transform: [[],[],['-5']]
                        },
                        {
                            id: '_24-1',
                            type: 'image',
                            rect: ['754px', '415px', '604px', '499px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "24-1.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 2145.0338485478,
                    autoPlay: true,
                    data: [
                        [
                            "eid95",
                            "skewX",
                            0,
                            1000,
                            "easeInOutQuad",
                            "${_24-3}",
                            '-5deg',
                            '5deg'
                        ],
                        [
                            "eid188",
                            "skewX",
                            1075,
                            1000,
                            "easeInOutQuad",
                            "${_24-3}",
                            '5deg',
                            '-5deg'
                        ],
                        [
                            "eid114",
                            "top",
                            0,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '-45px',
                            '-25px'
                        ],
                        [
                            "eid117",
                            "top",
                            500,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '-25px',
                            '-45px'
                        ],
                        [
                            "eid185",
                            "top",
                            1075,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '-45px',
                            '-25px'
                        ],
                        [
                            "eid184",
                            "top",
                            1575,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '-25px',
                            '-45px'
                        ],
                        [
                            "eid159",
                            "left",
                            0,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '1356px',
                            '1397px'
                        ],
                        [
                            "eid161",
                            "left",
                            500,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '1397px',
                            '1439px'
                        ],
                        [
                            "eid187",
                            "left",
                            1075,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '1439px',
                            '1397px'
                        ],
                        [
                            "eid186",
                            "left",
                            1575,
                            500,
                            "easeInOutQuad",
                            "${_24-3}",
                            '1397px',
                            '1356px'
                        ],
                        [
                            "eid113",
                            "top",
                            0,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '-45px',
                            '-25px'
                        ],
                        [
                            "eid116",
                            "top",
                            500,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '-25px',
                            '-45px'
                        ],
                        [
                            "eid180",
                            "top",
                            1075,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '-45px',
                            '-25px'
                        ],
                        [
                            "eid179",
                            "top",
                            1575,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '-25px',
                            '-45px'
                        ],
                        [
                            "eid94",
                            "skewX",
                            0,
                            1000,
                            "easeInOutQuad",
                            "${_24-2}",
                            '-5deg',
                            '5deg'
                        ],
                        [
                            "eid183",
                            "skewX",
                            1075,
                            1000,
                            "easeInOutQuad",
                            "${_24-2}",
                            '5deg',
                            '-5deg'
                        ],
                        [
                            "eid96",
                            "left",
                            0,
                            1000,
                            "easeInOutQuad",
                            "${_24-1}",
                            '754px',
                            '911px'
                        ],
                        [
                            "eid176",
                            "left",
                            1075,
                            1000,
                            "easeInOutQuad",
                            "${_24-1}",
                            '911px',
                            '754px'
                        ],
                        [
                            "eid158",
                            "left",
                            0,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '826px',
                            '867px'
                        ],
                        [
                            "eid160",
                            "left",
                            500,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '867px',
                            '909px'
                        ],
                        [
                            "eid182",
                            "left",
                            1075,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '909px',
                            '867px'
                        ],
                        [
                            "eid181",
                            "left",
                            1575,
                            500,
                            "easeInOutQuad",
                            "${_24-2}",
                            '867px',
                            '826px'
                        ],
                        [
                            "eid112",
                            "top",
                            0,
                            500,
                            "easeInOutQuad",
                            "${_24-1}",
                            '395px',
                            '415px'
                        ],
                        [
                            "eid115",
                            "top",
                            500,
                            500,
                            "easeInOutQuad",
                            "${_24-1}",
                            '415px',
                            '395px'
                        ],
                        [
                            "eid178",
                            "top",
                            1075,
                            500,
                            "easeInOutQuad",
                            "${_24-1}",
                            '395px',
                            '415px'
                        ],
                        [
                            "eid177",
                            "top",
                            1575,
                            500,
                            "easeInOutQuad",
                            "${_24-1}",
                            '415px',
                            '395px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_24");
