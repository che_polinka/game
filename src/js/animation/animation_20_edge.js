/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/20/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_20-2',
                            type: 'image',
                            rect: ['1576px', '284px', '281px', '747px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "20-2.png",'0px','0px']
                        },
                        {
                            id: '_20-1',
                            type: 'image',
                            rect: ['1576px', '284px', '305px', '761px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "20-1.png",'0px','0px']
                        },
                        {
                            id: '_20-3',
                            type: 'image',
                            rect: ['1576px', '284px', '301px', '774px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "20-3.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 6750,
                    autoPlay: true,
                    data: [
                        [
                            "eid92",
                            "opacity",
                            0,
                            250,
                            "linear",
                            "${_20-3}",
                            '1',
                            '0'
                        ],
                        [
                            "eid93",
                            "opacity",
                            4500,
                            250,
                            "linear",
                            "${_20-3}",
                            '0',
                            '1'
                        ],
                        [
                            "eid88",
                            "opacity",
                            2250,
                            250,
                            "linear",
                            "${_20-2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid91",
                            "opacity",
                            4500,
                            250,
                            "linear",
                            "${_20-2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid87",
                            "opacity",
                            0,
                            250,
                            "linear",
                            "${_20-1}",
                            '0',
                            '1'
                        ],
                        [
                            "eid90",
                            "opacity",
                            2250,
                            250,
                            "linear",
                            "${_20-1}",
                            '1',
                            '0'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_20");
