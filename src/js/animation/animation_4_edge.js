/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'question4img2',
                            display: 'block',
                            type: 'image',
                            rect: ['1368px', '-132px', '701px', '759px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"question4img2.png",'0px','0px']
                        },
                        {
                            id: 'question4img12',
                            type: 'image',
                            rect: ['602px', '202px', '1215px', '752px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"question4img1.png",'0px','0px']
                        },
                        {
                            id: 'question4img32',
                            type: 'image',
                            rect: ['1010px', '1125px', '460px', '1083px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"question4img3.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 2034,
                    autoPlay: true,
                    data: [
                        [
                            "eid21",
                            "opacity",
                            250,
                            364,
                            "easeOutBack",
                            "${question4img2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid18",
                            "top",
                            500,
                            500,
                            "easeOutBack",
                            "${question4img32}",
                            '1125px',
                            '118px'
                        ],
                        [
                            "eid20",
                            "top",
                            250,
                            364,
                            "easeOutBack",
                            "${question4img2}",
                            '-132px',
                            '274px'
                        ],
                        [
                            "eid24",
                            "display",
                            985,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid25",
                            "display",
                            1019,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid26",
                            "display",
                            1062,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid27",
                            "display",
                            1114,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid28",
                            "display",
                            1152,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid31",
                            "display",
                            1364,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid33",
                            "display",
                            1614,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid35",
                            "display",
                            1653,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid36",
                            "display",
                            2000,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid37",
                            "display",
                            2034,
                            0,
                            "easeOutBack",
                            "${question4img2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid19",
                            "left",
                            250,
                            364,
                            "easeOutBack",
                            "${question4img2}",
                            '1368px',
                            '889px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_4");
