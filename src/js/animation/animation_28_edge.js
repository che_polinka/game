/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/28/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_28-1',
                            type: 'image',
                            rect: ['1043px', '1144px', '476px', '734px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "28-1.png",'0px','0px']
                        },
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            type: 'rect',
                            rect: ['1961px', '531', '290', '198', 'auto', 'auto']
                        },
                        {
                            id: '_28-2',
                            type: 'image',
                            rect: ['2004px', '410px', '622px', '678px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "28-2.png",'0px','0px']
                        },
                        {
                            id: '_28-4',
                            type: 'image',
                            rect: ['822px', '-522px', '927px', '467px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "28-4.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid202",
                            "top",
                            1000,
                            1000,
                            "easeOutElastic",
                            "${_28-4}",
                            '-522px',
                            '630px'
                        ],
                        [
                            "eid198",
                            "left",
                            2000,
                            500,
                            "easeOutCubic",
                            "${Symbol_1}",
                            '1961px',
                            '1295px'
                        ],
                        [
                            "eid196",
                            "left",
                            2000,
                            500,
                            "easeOutCubic",
                            "${_28-2}",
                            '2004px',
                            '1338px'
                        ],
                        [
                            "eid204",
                            "top",
                            500,
                            500,
                            "easeOutBack",
                            "${_28-1}",
                            '1144px',
                            '346px'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '290px', '198px', 'auto', 'auto'],
                            id: '_28-3',
                            opacity: '0',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)',im + '28-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '290px', '198px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid191",
                            "opacity",
                            0,
                            1500,
                            "linear",
                            "${_28-3}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid192",
                            "opacity",
                            1500,
                            1500,
                            "linear",
                            "${_28-3}",
                            '1',
                            '0'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_28");
