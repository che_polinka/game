/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/26/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_26-1',
                            type: 'image',
                            rect: ['398px', '-510px', '1600px', '480px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "26-1.png",'0px','0px']
                        },
                        {
                            id: '_26-2',
                            type: 'image',
                            rect: ['1182px', '1138px', '85px', '129px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "26-2.png",'0px','0px']
                        },
                        {
                            id: '_26-3',
                            type: 'image',
                            rect: ['686px', '660px', '298px', '219px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "26-3.png",'0px','0px']
                        },
                        {
                            id: '_26-4',
                            type: 'image',
                            rect: ['92px', '1233px', '1652px', '71px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "26-4.png",'0px','0px']
                        },
                        {
                            id: '_26-5',
                            type: 'image',
                            rect: ['1240px', '410px', '394px', '281px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "26-5.png",'0px','0px']
                        },
                        {
                            id: 'Symbol_2',
                            symbolName: 'Symbol_2',
                            display: 'none',
                            type: 'rect',
                            rect: ['542', '636', '293', '66', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            display: 'none',
                            type: 'rect',
                            rect: ['1486', '592', '295', '56', 'auto', 'auto'],
                            opacity: '0'
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 15000,
                    autoPlay: true,
                    data: [
                        [
                            "eid95",
                            "top",
                            750,
                            500,
                            "easeOutBack",
                            "${_26-1}",
                            '-510px',
                            '610px'
                        ],
                        [
                            "eid101",
                            "top",
                            1000,
                            500,
                            "easeOutCubic",
                            "${_26-3}",
                            '660px',
                            '826px'
                        ],
                        [
                            "eid114",
                            "opacity",
                            1000,
                            3000,
                            "linear",
                            "${Symbol_1}",
                            '0',
                            '1'
                        ],
                        [
                            "eid107",
                            "display",
                            0,
                            0,
                            "easeOutCubic",
                            "${Symbol_2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid109",
                            "display",
                            1009,
                            0,
                            "easeOutCubic",
                            "${Symbol_2}",
                            'block',
                            'block'
                        ],
                        [
                            "eid94",
                            "top",
                            250,
                            500,
                            "easeOutBack",
                            "${_26-4}",
                            '1233px',
                            '1009px'
                        ],
                        [
                            "eid115",
                            "opacity",
                            1000,
                            3000,
                            "linear",
                            "${Symbol_2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid100",
                            "opacity",
                            1000,
                            500,
                            "easeOutCubic",
                            "${_26-3}",
                            '0',
                            '1'
                        ],
                        [
                            "eid99",
                            "top",
                            1250,
                            750,
                            "easeOutCubic",
                            "${_26-2}",
                            '1138px',
                            '916px'
                        ],
                        [
                            "eid119",
                            "opacity",
                            994,
                            3000,
                            "linear",
                            "${_26-5}",
                            '0',
                            '1'
                        ],
                        [
                            "eid118",
                            "display",
                            1003,
                            0,
                            "easeOutCubic",
                            "${_26-5}",
                            'block',
                            'block'
                        ],
                        [
                            "eid106",
                            "display",
                            0,
                            0,
                            "easeOutCubic",
                            "${Symbol_1}",
                            'none',
                            'block'
                        ],
                        [
                            "eid108",
                            "display",
                            1009,
                            0,
                            "easeOutCubic",
                            "${Symbol_1}",
                            'block',
                            'block'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_26-7',
                            type: 'image',
                            rect: ['40px', '0px', '295px', '56px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '26-7.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '295px', '56px']
                        }
                    }
                },
                timeline: {
                    duration: 15000,
                    autoPlay: true,
                    data: [
                        [
                            "eid121",
                            "left",
                            0,
                            7500,
                            "linear",
                            "${_26-7}",
                            '0px',
                            '-100px'
                        ],
                        [
                            "eid120",
                            "left",
                            7500,
                            7500,
                            "linear",
                            "${_26-7}",
                            '-100px',
                            '40px'
                        ]
                    ]
                }
            },
            "Symbol_2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_26-6',
                            type: 'image',
                            rect: ['0px', '0px', '293px', '66px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '26-6.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '293px', '66px']
                        }
                    }
                },
                timeline: {
                    duration: 15000,
                    autoPlay: true,
                    data: [
                        [
                            "eid117",
                            "left",
                            0,
                            7500,
                            "linear",
                            "${_26-6}",
                            '0px',
                            '-100px'
                        ],
                        [
                            "eid116",
                            "left",
                            7500,
                            7500,
                            "linear",
                            "${_26-6}",
                            '-100px',
                            '40px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_26");
