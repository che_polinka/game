/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/8/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_1-8',
                            type: 'image',
                            rect: ['-1258px', '1102px', '1862px', '1371px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "1-8.png",'0px','0px']
                        },
                        {
                            id: '_2-8',
                            type: 'image',
                            rect: ['128px', '68px', '1862px', '1371px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "2-8.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 43500,
                    autoPlay: true,
                    data: [
                        [
                            "eid20",
                            "left",
                            500,
                            2500,
                            "easeOutQuint",
                            "${_2-8}",
                            '1930px',
                            '128px'
                        ],
                        [
                            "eid26",
                            "left",
                            3250,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '128px',
                            '320px'
                        ],
                        [
                            "eid33",
                            "left",
                            8625,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '320px',
                            '128px'
                        ],
                        [
                            "eid38",
                            "left",
                            18000,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '128px',
                            '320px'
                        ],
                        [
                            "eid39",
                            "left",
                            23375,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '320px',
                            '128px'
                        ],
                        [
                            "eid46",
                            "left",
                            32750,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '128px',
                            '320px'
                        ],
                        [
                            "eid47",
                            "left",
                            38125,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '320px',
                            '128px'
                        ],
                        [
                            "eid18",
                            "left",
                            500,
                            2500,
                            "easeOutQuint",
                            "${_1-8}",
                            '-1258px',
                            '338px'
                        ],
                        [
                            "eid28",
                            "left",
                            3250,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '338px',
                            '164px'
                        ],
                        [
                            "eid36",
                            "left",
                            8625,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '164px',
                            '338px'
                        ],
                        [
                            "eid42",
                            "left",
                            18000,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '338px',
                            '164px'
                        ],
                        [
                            "eid43",
                            "left",
                            23375,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '164px',
                            '338px'
                        ],
                        [
                            "eid50",
                            "left",
                            32750,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '338px',
                            '164px'
                        ],
                        [
                            "eid51",
                            "left",
                            38125,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '164px',
                            '338px'
                        ],
                        [
                            "eid25",
                            "opacity",
                            749,
                            1831,
                            "easeOutQuint",
                            "${_1-8}",
                            '0',
                            '1'
                        ],
                        [
                            "eid23",
                            "opacity",
                            752,
                            1831,
                            "easeOutQuint",
                            "${_2-8}",
                            '0',
                            '1'
                        ],
                        [
                            "eid21",
                            "top",
                            500,
                            2500,
                            "easeOutQuint",
                            "${_2-8}",
                            '-1018px',
                            '68px'
                        ],
                        [
                            "eid27",
                            "top",
                            3250,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '68px',
                            '-60px'
                        ],
                        [
                            "eid32",
                            "top",
                            8625,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '-60px',
                            '68px'
                        ],
                        [
                            "eid40",
                            "top",
                            18000,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '68px',
                            '-60px'
                        ],
                        [
                            "eid41",
                            "top",
                            23375,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '-60px',
                            '68px'
                        ],
                        [
                            "eid48",
                            "top",
                            32750,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '68px',
                            '-60px'
                        ],
                        [
                            "eid49",
                            "top",
                            38125,
                            5375,
                            "easeInOutQuad",
                            "${_2-8}",
                            '-60px',
                            '68px'
                        ],
                        [
                            "eid19",
                            "top",
                            500,
                            2500,
                            "easeOutQuint",
                            "${_1-8}",
                            '1102px',
                            '-60px'
                        ],
                        [
                            "eid29",
                            "top",
                            3250,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '-60px',
                            '80px'
                        ],
                        [
                            "eid37",
                            "top",
                            8625,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '80px',
                            '-60px'
                        ],
                        [
                            "eid44",
                            "top",
                            18000,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '-60px',
                            '80px'
                        ],
                        [
                            "eid45",
                            "top",
                            23375,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '80px',
                            '-60px'
                        ],
                        [
                            "eid52",
                            "top",
                            32750,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '-60px',
                            '80px'
                        ],
                        [
                            "eid53",
                            "top",
                            38125,
                            5375,
                            "easeInOutQuad",
                            "${_1-8}",
                            '80px',
                            '-60px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_8");
