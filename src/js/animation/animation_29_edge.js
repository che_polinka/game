/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/29/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_29-1',
                            type: 'image',
                            rect: ['792px', '1118px', '1133px', '524px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "29-1.png",'0px','0px']
                        },
                        {
                            id: '_29-2',
                            display: 'none',
                            type: 'image',
                            rect: ['1258', '368px', '478px', '776px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im + "29-2.png",'0px','0px'],
                            transform: [[],[],[],['0.4','0.4']]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 1500,
                    autoPlay: true,
                    data: [
                        [
                            "eid191",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${_29-2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid190",
                            "scaleY",
                            500,
                            500,
                            "easeOutBack",
                            "${_29-2}",
                            '0.4',
                            '1'
                        ],
                        [
                            "eid193",
                            "top",
                            1000,
                            500,
                            "easeOutBack",
                            "${_29-1}",
                            '1118px',
                            '648px'
                        ],
                        [
                            "eid189",
                            "scaleX",
                            500,
                            500,
                            "easeOutBack",
                            "${_29-2}",
                            '0.4',
                            '1'
                        ],
                        [
                            "eid194",
                            "display",
                            0,
                            0,
                            "linear",
                            "${_29-2}",
                            'none',
                            'none'
                        ],
                        [
                            "eid195",
                            "display",
                            500,
                            0,
                            "linear",
                            "${_29-2}",
                            'none',
                            'block'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_29");
