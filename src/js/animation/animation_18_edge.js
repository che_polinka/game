/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/18/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_18-4',
                            type: 'image',
                            rect: ['799px', '473px', '1121px', '609px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "18-4.png",'0px','0px']
                        },
                        {
                            id: 'Symbol_3',
                            symbolName: 'Symbol_3',
                            type: 'rect',
                            rect: ['946', '778', '46', '45', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_4',
                            symbolName: 'Symbol_4',
                            type: 'rect',
                            rect: ['877', '1071', '46', '45', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_5',
                            symbolName: 'Symbol_5',
                            type: 'rect',
                            rect: ['1705', '726', '46', '45', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_6',
                            symbolName: 'Symbol_6',
                            type: 'rect',
                            rect: ['1686', '1008', '46', '45', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_7',
                            symbolName: 'Symbol_7',
                            type: 'rect',
                            rect: ['1401', '592', '46', '45', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_9',
                            symbolName: 'Symbol_9',
                            type: 'rect',
                            rect: ['713', '511', '316', '316', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_8',
                            symbolName: 'Symbol_8',
                            type: 'rect',
                            rect: ['1463px', '333px', '330', '283', 'auto', 'auto'],
                            opacity: '0'
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [
                        [
                            "eid134",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${Symbol_9}",
                            '0',
                            '1'
                        ],
                        [
                            "eid135",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${Symbol_4}",
                            '0',
                            '1'
                        ],
                        [
                            "eid138",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${Symbol_6}",
                            '0',
                            '1'
                        ],
                        [
                            "eid132",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${Symbol_7}",
                            '0',
                            '1'
                        ],
                        [
                            "eid137",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${Symbol_8}",
                            '0',
                            '1'
                        ],
                        [
                            "eid133",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${_18-4}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid139",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${Symbol_3}",
                            '0',
                            '1'
                        ],
                        [
                            "eid136",
                            "opacity",
                            0,
                            500,
                            "easeInOutQuad",
                            "${Symbol_5}",
                            '0',
                            '1'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_18-3',
                            type: 'image',
                            rect: ['0px', '0px', '46px', '45px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '18-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '46px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "Symbol_2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_18-3Copy',
                            type: 'image',
                            rect: ['0px', '0px', '46px', '45px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '18-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '46px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "Symbol_3": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: '_18-32',
                            opacity: '1',
                            rect: ['-31px', '-32px', '46px', '45px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '18-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '46px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid76",
                            "left",
                            500,
                            1500,
                            "linear",
                            "${_18-32}",
                            '-31px',
                            '238px'
                        ],
                        [
                            "eid109",
                            "opacity",
                            1523,
                            477,
                            "linear",
                            "${_18-32}",
                            '1',
                            '0'
                        ],
                        [
                            "eid77",
                            "top",
                            500,
                            1500,
                            "linear",
                            "${_18-32}",
                            '-32px',
                            '238px'
                        ]
                    ]
                }
            },
            "Symbol_4": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '46px', '45px', 'auto', 'auto'],
                            type: 'image',
                            id: '_18-33',
                            opacity: '1',
                            display: 'block',
                            fill: ['rgba(0,0,0,0)', im + '18-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '46px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid84",
                            "left",
                            1000,
                            2000,
                            "linear",
                            "${_18-33}",
                            '0px',
                            '196px'
                        ],
                        [
                            "eid87",
                            "display",
                            3000,
                            0,
                            "linear",
                            "${_18-33}",
                            'block',
                            'none'
                        ],
                        [
                            "eid141",
                            "opacity",
                            1000,
                            0,
                            "linear",
                            "${_18-33}",
                            '1',
                            '1'
                        ],
                        [
                            "eid88",
                            "opacity",
                            2631,
                            369,
                            "linear",
                            "${_18-33}",
                            '1',
                            '0'
                        ],
                        [
                            "eid85",
                            "top",
                            1000,
                            2000,
                            "linear",
                            "${_18-33}",
                            '0px',
                            '-200px'
                        ]
                    ]
                }
            },
            "Symbol_5": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: '_18-34',
                            opacity: '1',
                            rect: ['-42px', '-218px', '46px', '45px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '18-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '46px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid92",
                            "top",
                            2000,
                            1750,
                            "linear",
                            "${_18-34}",
                            '-218px',
                            '45px'
                        ],
                        [
                            "eid93",
                            "opacity",
                            3446,
                            304,
                            "linear",
                            "${_18-34}",
                            '1',
                            '0'
                        ],
                        [
                            "eid91",
                            "left",
                            2000,
                            1750,
                            "linear",
                            "${_18-34}",
                            '219px',
                            '-42px'
                        ]
                    ]
                }
            },
            "Symbol_6": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: '_18-35',
                            opacity: '0',
                            rect: ['-59px', '-59px', '46px', '45px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '18-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '46px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid96",
                            "top",
                            2000,
                            500,
                            "linear",
                            "${_18-35}",
                            '0px',
                            '-59px'
                        ],
                        [
                            "eid99",
                            "top",
                            2500,
                            500,
                            "linear",
                            "${_18-35}",
                            '-59px',
                            '-112px'
                        ],
                        [
                            "eid95",
                            "left",
                            2000,
                            500,
                            "linear",
                            "${_18-35}",
                            '0px',
                            '-59px'
                        ],
                        [
                            "eid98",
                            "left",
                            2500,
                            500,
                            "linear",
                            "${_18-35}",
                            '-59px',
                            '-111px'
                        ],
                        [
                            "eid97",
                            "opacity",
                            2000,
                            500,
                            "linear",
                            "${_18-35}",
                            '0',
                            '1'
                        ],
                        [
                            "eid100",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${_18-35}",
                            '1',
                            '0'
                        ]
                    ]
                }
            },
            "Symbol_7": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            rect: ['27px', '33px', '46px', '45px', 'auto', 'auto'],
                            id: '_18-36',
                            opacity: '0',
                            display: 'block',
                            fill: ['rgba(0,0,0,0)', im + '18-3.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '46px', '45px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid102",
                            "top",
                            2500,
                            500,
                            "linear",
                            "${_18-36}",
                            '33px',
                            '-28px'
                        ],
                        [
                            "eid105",
                            "top",
                            3000,
                            500,
                            "linear",
                            "${_18-36}",
                            '-28px',
                            '-95px'
                        ],
                        [
                            "eid103",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${_18-36}",
                            '0',
                            '1'
                        ],
                        [
                            "eid106",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${_18-36}",
                            'block',
                            'none'
                        ],
                        [
                            "eid101",
                            "left",
                            2500,
                            500,
                            "linear",
                            "${_18-36}",
                            '-33px',
                            '27px'
                        ],
                        [
                            "eid104",
                            "left",
                            3000,
                            500,
                            "linear",
                            "${_18-36}",
                            '27px',
                            '96px'
                        ]
                    ]
                }
            },
            "Symbol_8": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '330px', '283px', 'auto', 'auto'],
                            id: '_18-2',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', im + '18-2.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '330px', '283px']
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [
                        [
                            "eid116",
                            "left",
                            0,
                            3000,
                            "easeInOutQuad",
                            "${_18-2}",
                            '0px',
                            '-18px'
                        ],
                        [
                            "eid117",
                            "left",
                            3000,
                            3000,
                            "easeInOutQuad",
                            "${_18-2}",
                            '-18px',
                            '0px'
                        ],
                        [
                            "eid118",
                            "top",
                            0,
                            3000,
                            "easeInOutQuad",
                            "${_18-2}",
                            '0px',
                            '-42px'
                        ],
                        [
                            "eid119",
                            "top",
                            3000,
                            3000,
                            "easeInOutQuad",
                            "${_18-2}",
                            '-42px',
                            '0px'
                        ]
                    ]
                }
            },
            "Symbol_9": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '316px', '316px', 'auto', 'auto'],
                            id: '_18-1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', im + '18-1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '316px', '316px']
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [
                        [
                            "eid122",
                            "top",
                            0,
                            3000,
                            "easeInOutQuad",
                            "${_18-1}",
                            '0px',
                            '34px'
                        ],
                        [
                            "eid123",
                            "top",
                            3000,
                            3000,
                            "easeInOutQuad",
                            "${_18-1}",
                            '34px',
                            '0px'
                        ],
                        [
                            "eid120",
                            "left",
                            0,
                            3000,
                            "easeInOutQuad",
                            "${_18-1}",
                            '0px',
                            '-18px'
                        ],
                        [
                            "eid121",
                            "left",
                            3000,
                            3000,
                            "easeInOutQuad",
                            "${_18-1}",
                            '-18px',
                            '0px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_18");
