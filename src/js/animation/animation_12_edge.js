/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/12/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_12-1',
                            type: 'image',
                            rect: ['721px', '1280px', '1394px', '714px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "12-1.png",'0px','0px']
                        },
                        {
                            id: '_12-4',
                            type: 'image',
                            rect: ['1543px', '1173px', '264px', '797px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "12-4.png",'0px','0px']
                        },
                        {
                            id: '_12-3',
                            type: 'image',
                            rect: ['1156px', '1122px', '355px', '817px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "12-3.png",'0px','0px']
                        },
                        {
                            id: '_12-2',
                            type: 'image',
                            rect: ['833px', '1161px', '295px', '778px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "12-2.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 2250,
                    autoPlay: true,
                    data: [
                        [
                            "eid20",
                            "top",
                            1250,
                            500,
                            "easeOutBack",
                            "${_12-4}",
                            '1173px',
                            '403px'
                        ],
                        [
                            "eid22",
                            "top",
                            750,
                            500,
                            "easeOutBack",
                            "${_12-2}",
                            '1161px',
                            '400px'
                        ],
                        [
                            "eid21",
                            "top",
                            1750,
                            500,
                            "easeOutBack",
                            "${_12-3}",
                            '1122px',
                            '361px'
                        ],
                        [
                            "eid19",
                            "top",
                            250,
                            500,
                            "easeOutBack",
                            "${_12-1}",
                            '1280px',
                            '519px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_12");
