/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/25/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_25-1',
                            type: 'image',
                            rect: ['514px', '363px', '1406px', '717px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "25-1.png",'0px','0px']
                        },
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            type: 'rect',
                            rect: ['1409px', '505px', '139', '139', 'auto', 'auto'],
                            opacity: '0'
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid102",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${Symbol_1}",
                            '0',
                            '1'
                        ],
                        [
                            "eid101",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${_25-1}",
                            '0.000000',
                            '1'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '139px', '139px', 'auto', 'auto'],
                            id: '_25-2',
                            opacity: '0',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', im + '25-2.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '139px', '139px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid94",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${_25-2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid96",
                            "opacity",
                            1000,
                            500,
                            "linear",
                            "${_25-2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid97",
                            "opacity",
                            2000,
                            500,
                            "linear",
                            "${_25-2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid98",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${_25-2}",
                            '1',
                            '0'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_25");
