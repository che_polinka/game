/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/23/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_23-22',
                            type: 'image',
                            rect: ['1169px', '-741px', '595px', '705px', 'auto', 'auto'],
                            clip: 'rect(-62px 595px 705px 0px)',
                            fill: ["rgba(0,0,0,0)",im + "23-22.png",'0px','0px']
                        },
                        {
                            id: '_23-12',
                            type: 'image',
                            rect: ['829px', '439px', '304px', '685px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im + "23-12.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 2250,
                    autoPlay: true,
                    data: [
                        [
                            "eid97",
                            "top",
                            1250,
                            1000,
                            "easeOutBounce",
                            "${_23-22}",
                            '-741px',
                            '427px'
                        ],
                        [
                            "eid94",
                            "top",
                            500,
                            500,
                            "easeOutBack",
                            "${_23-12}",
                            '1109px',
                            '439px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_23");
