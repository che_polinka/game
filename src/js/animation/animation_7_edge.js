/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/7/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            type: 'rect',
                            rect: ['auto', '256px', '1116', '1088', '69px', 'auto'],
                            transform: [[],[],[],['-1.00039']]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid40",
                            "scaleX",
                            750,
                            173,
                            "linear",
                            "${Symbol_1}",
                            '1',
                            '0.16129'
                        ],
                        [
                            "eid41",
                            "scaleX",
                            923,
                            160,
                            "linear",
                            "${Symbol_1}",
                            '0.16129',
                            '-1.00039'
                        ],
                        [
                            "eid43",
                            "scaleX",
                            1083,
                            173,
                            "linear",
                            "${Symbol_1}",
                            '1',
                            '0.16129'
                        ],
                        [
                            "eid44",
                            "scaleX",
                            1256,
                            160,
                            "linear",
                            "${Symbol_1}",
                            '0.16129',
                            '-1.00039'
                        ],
                        [
                            "eid57",
                            "scaleX",
                            1416,
                            173,
                            "linear",
                            "${Symbol_1}",
                            '1',
                            '0.16129'
                        ],
                        [
                            "eid58",
                            "scaleX",
                            1589,
                            160,
                            "linear",
                            "${Symbol_1}",
                            '0.16129',
                            '-1.00039'
                        ],
                        [
                            "eid21",
                            "top",
                            750,
                            999,
                            "easeOutQuad",
                            "${Symbol_1}",
                            '1106px',
                            '256px'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_1',
                            type: 'image',
                            rect: ['0px', '-40px', '1116px', '1088px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', im + '1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '1116px', '1088px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid18",
                            "top",
                            0,
                            1500,
                            "easeInOutQuad",
                            "${_1}",
                            '0px',
                            '-40px'
                        ],
                        [
                            "eid20",
                            "top",
                            1500,
                            1500,
                            "easeInOutQuad",
                            "${_1}",
                            '-40px',
                            '0px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_7");
