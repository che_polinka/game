/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='img/animation/13/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_13-3',
                            display: 'none',
                            type: 'image',
                            rect: ['898px', '399px', '854px', '681px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im + "13-3.png",'0px','0px']
                        },
                        {
                            id: 'Symbol_2',
                            symbolName: 'Symbol_2',
                            display: 'none',
                            type: 'rect',
                            rect: ['981', '445px', '687', '549', 'auto', 'auto'],
                            opacity: '0'
                        },
                        {
                            id: 'Symbol_1',
                            symbolName: 'Symbol_1',
                            display: 'none',
                            type: 'rect',
                            rect: ['1086px', '579px', '477', '158', 'auto', 'auto'],
                            opacity: '0'
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1920px', '1080px', 'auto', 'auto'],
                            sizeRange: ['0px','1920px','',''],
                            overflow: 'hidden',
                            fill: ["rgba(143,51,51,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid25",
                            "display",
                            0,
                            0,
                            "easeOutBack",
                            "${Symbol_2}",
                            'none',
                            'none'
                        ],
                        [
                            "eid28",
                            "display",
                            1000,
                            0,
                            "easeOutBack",
                            "${Symbol_2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid36",
                            "opacity",
                            500,
                            1000,
                            "easeOutBack",
                            "${_13-3}",
                            '0',
                            '1'
                        ],
                        [
                            "eid37",
                            "opacity",
                            1000,
                            1000,
                            "easeOutBack",
                            "${Symbol_2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid24",
                            "display",
                            0,
                            0,
                            "easeOutBack",
                            "${Symbol_1}",
                            'none',
                            'none'
                        ],
                        [
                            "eid27",
                            "display",
                            1000,
                            0,
                            "easeOutBack",
                            "${Symbol_1}",
                            'none',
                            'block'
                        ],
                        [
                            "eid35",
                            "opacity",
                            1000,
                            1000,
                            "easeOutBack",
                            "${Symbol_1}",
                            '0',
                            '1'
                        ],
                        [
                            "eid23",
                            "display",
                            0,
                            0,
                            "easeOutBack",
                            "${_13-3}",
                            'none',
                            'none'
                        ],
                        [
                            "eid26",
                            "display",
                            500,
                            0,
                            "easeOutBack",
                            "${_13-3}",
                            'none',
                            'block'
                        ]
                    ]
                }
            },
            "Symbol_1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '477px', '158px', 'auto', 'auto'],
                            id: '_13-1',
                            type: 'image',
                            clip: 'rect(0px 480px 158px 480px)',
                            fill: ['rgba(0,0,0,0)', im + '13-1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '477px', '158px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid42",
                            "clip",
                            500,
                            1500,
                            "easeOutQuad",
                            "${_13-1}",
                            [0,0,158,0],
                            [0,480,158,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid43",
                            "clip",
                            2500,
                            1500,
                            "easeOutQuad",
                            "${_13-1}",
                            [0,480,158,0],
                            [0,480,158,480],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ]
                    ]
                }
            },
            "Symbol_2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '687px', '549px', 'auto', 'auto'],
                            id: '_13-2',
                            transform: [[], [], [], ['0.9', '0.9']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', im + '13-2.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '687px', '549px']
                        }
                    }
                },
                timeline: {
                    duration: 1500,
                    autoPlay: true,
                    data: [
                        [
                            "eid49",
                            "scaleY",
                            500,
                            500,
                            "easeOutBack",
                            "${_13-2}",
                            '0.9',
                            '1.1'
                        ],
                        [
                            "eid52",
                            "scaleY",
                            1000,
                            500,
                            "easeOutBack",
                            "${_13-2}",
                            '1.1',
                            '0.9'
                        ],
                        [
                            "eid48",
                            "scaleX",
                            500,
                            500,
                            "easeOutBack",
                            "${_13-2}",
                            '0.9',
                            '1.1'
                        ],
                        [
                            "eid53",
                            "scaleX",
                            1000,
                            500,
                            "easeOutBack",
                            "${_13-2}",
                            '1.1',
                            '0.9'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

})("animation_13");
