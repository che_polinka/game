/**
 * Ajax registration and/or authorization new user.
 */
;(function(window) {
    'use strict';

    var
        document = window.document
        , jQuery = $ = window.jQuery
        ;

    /**
     * Redirect to next page.
     */
    function redirect() {
        // If the application is running out of "nginx",
        // merging current href and hash.
        var withoutHash = window.location.href.split('#')[0].replace(/\/$/, "")

        window.location.href = withoutHash + '/#/level/'
    }

    function switchError(form, path, message) {
        var selector = path ? '[id=' + path + ']' : '[id]';

        $(selector, form).next('div.error').html(message || '')[message ? 'show' : 'hide']()
    }

    function resetForm(elem) {
        try { elem.reset() }
        catch (e) { elem[0].reset() }
    }

    $(document.forms['auth']).on('submit', function() {
        var
            form = $(this)
            , dataAuth = $('[data-auth]', form)
            ;

        $.ajax({
            url: '/login',
            method: 'POST',
            data: form.serialize(),
            complete: function(jqXHR, textStatus) {
            },
            statusCode: {
                200: function(data, textStatus, jqXHR) {
                    switchError(form);
                    resetForm(form);
                    redirect();
                },
                201: function(data, textStatus, jqXHR) {
                    switchError(form);
                    resetForm(form);
                    redirect();
                },
                403: function(jqXHR, textStatus, errorThrown) {
                    switchError(form);

                    try {
                        var errors = $.parseJSON(jqXHR.responseText);

                        $.each(errors, function(i, error) {
                            error.path && switchError(form, error.path, error.message);
                        });
                    } catch (e) {
                        // pass
                    }

                }
            }
        });

        return false;
    });

})(this);
