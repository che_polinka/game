(function(root, factory) {
    // Browser globals.
    root.Game = factory();

}(this, function() {

    'use strict';

    var Game;

    var io = window.io,
        socket = io.connect();

    var SECTION_SELECTOR = '.container .slides section',
        SECTION_QUESTION_SELECTOR = '.container .slides section[data-question]',
        NAVIGATION_SELECTOR = '.container .slides section[data-question] li',
        TIMER_DIV = '.timer + .popup .time',
        CONTROLS = '.controls',

        config = {

            // The "normal" size of the presentation, aspect ratio will be preserved
            // when the presentation is scaled to fit different resolutions
            width: 1920,
            height: 1080,

            // Factor of the display size that should remain empty around the content
            margin: 0.1,

            // Bounds for smallest/largest possible scale to apply to content
            minScale: 0.2,
            maxScale: 1.5,

            // Vertical centering of slides
            center: true,

            gameTime: 0,
            lives: 5,

            // Randomizes the order of questions each time the presentation loads
            shuffle: false
        },
        dom = {},

        indexh,
        indexUser,

    // Question index
        questionIndex = 0,

        livesCount,

    // level of difficulty. sets count of question
        difficulty = {
            easy : 10,
            hard : 20
        },

    // default difficulty value
        level,

    // game state. includes objects of answered questions with answers and time
        state = [],

        noMorePrevStat = false,
        noMoreNextStat = false,

        topStatId,
        topRating,

        bottomStatId,
        bottomRating,

    // The current scale of the presentation (see width/height config)
        scale = 1,

    // Count of questions, depends on level
        questionCount,

    // Delays go to next slide
        nextSlideTimeout = 0,

    // The current slide HTML elements
        currentSlide,

    // Flag if the popup that there is few time left was shown
        fewTimeFlag = false,

        timerID = 0,

        loaded = false,

    // CSS transform that is currently applied to the slides container,
    // split into two groups
        slidesTransform = { layout: '', overview: '' },

    // Flag if the quiz was started
        gameStarted = false,

        slidesStarted = false,

        gameOverFlag = false;

    function initialize( options ) {

        // Cache references to key DOM elements
        dom.wrapper = document.querySelector( '.container' );
        dom.slides = dom.wrapper.querySelector('.container .slides');

        dom.background = document.querySelector('.background');

        // All sections selector
        dom.sections = toArray(document.querySelectorAll( SECTION_SELECTOR ));
        dom.controls = document.querySelector(CONTROLS);

        // Copy options over to our config object
        extend( config, options );

        questionCount = difficulty[level];

        // Force a layout when the whole page, has loaded
        window.addEventListener( 'load', layout, false );

        start();

    }

    /**
     * Starts up by binding input events and navigating
     * to the current URL deeplink if there is one.
     */
    function start() {

        // Make sure we've got all the DOM elements we need
        setupDOM();

        // Hide the controls bar
        hideControlsBar();

        // Updates the presentation to match the current configuration values
        //configure();

        removeEventListeners();
        addEventListeners();

        addSocketListener();

        // Read the initial hash
        readURL();

        // Notify listeners that the presentation is ready but use a 1ms
        // timeout to ensure it's not fired synchronously after #initialize()
        setTimeout( function() {
            // Enable transitions now that we're loaded
            loaded = true;

        }, 1 );
    }


    /**
     * Applies JavaScript-controlled layout rules to the
     * presentation.
     */
    function layout() {

        if(dom.wrapper) {

            dom.wrapper.removeAttribute('hidden');
            dom.wrapper.removeAttribute('aria-hidden');

            // blur background
            dom.background.style.webkitFilter = 'blur(6px)';
            dom.background.style.filter = 'blur(6px)';
            dom.background.style.mozFilter = 'blur(6px)';
            dom.background.style.oFilter = 'blur(6px)';

            var size = getComputedSlideSize();

            dom.slides.style.width = size.width + 'px';
            dom.slides.style.height = size.height + 'px';

            // Determine scale of content to fit within available space
            scale = Math.min( size.presentationWidth / size.width, size.presentationHeight / size.height );

            // Respect max/min scale settings
            scale = Math.max( scale, config.minScale );
            scale = Math.min( scale, config.maxScale );

            // Don't apply any scaling styles if scale is 1
            if( scale === 1 ) {
                dom.slides.style.zoom = '';
                dom.slides.style.left = '';
                dom.slides.style.top = '';
                dom.slides.style.bottom = '';
                dom.slides.style.right = '';
                transformSlides( { layout: '' } );
            }
            else {
                // Prefer zoom for scaling up so that content remains crisp.
                // Don't use zoom to scale down since that can lead to shifts
                // in text layout/line breaks.
                if( scale > 1) {
                    dom.slides.style.zoom = scale;
                    dom.slides.style.left = '';
                    dom.slides.style.top = '';
                    dom.slides.style.bottom = '';
                    dom.slides.style.right = '';
                    transformSlides( { layout: '' } );
                }
                // Apply scale transform as a fallback
                else {
                    dom.slides.style.zoom = '';
                    dom.slides.style.left = '50%';
                    dom.slides.style.top = '50%';
                    dom.slides.style.bottom = 'auto';
                    dom.slides.style.right = 'auto';
                    transformSlides( { layout: 'translate(-50%, -50%) scale('+ scale +')' } );
                    if (window.innerWidth < 380)
                        transformSlides( { layout: 'translate(-50%, -50%) scale('+ scale +') rotate(90deg)' } );
                }
            }

            // Select all slides
            var slides = toArray( dom.wrapper.querySelectorAll( SECTION_SELECTOR ) );

            for( var i = 0, len = slides.length; i < len; i++ ) {
                var slide = slides[ i ];

                // Don't bother updating invisible slides
                if( slide.style.display === 'none' ) {
                    continue;
                }

                slide.style.top = '';

            }

        }

    }

    /**
     * Calculates the computed pixel size of our slides. These
     * values are based on the width and height configuration
     * options.
     */
    function getComputedSlideSize( presentationWidth, presentationHeight ) {

        var size = {
            // Slide size
            width: config.width,
            height: config.height,

            // Presentation size
            presentationWidth: presentationWidth || dom.wrapper.offsetWidth,
            presentationHeight: presentationHeight || dom.wrapper.offsetHeight
        };

        // Reduce available space by margin
        size.presentationWidth -= ( size.presentationWidth * config.margin );
        size.presentationHeight -= ( size.presentationHeight * config.margin );

        // Slide width may be a percentage of available width
        if( typeof size.width === 'string' && /%$/.test( size.width ) ) {
            size.width = parseInt( size.width, 10 ) / 100 * size.presentationWidth;
        }

        // Slide height may be a percentage of available height
        if( typeof size.height === 'string' && /%$/.test( size.height ) ) {
            size.height = parseInt( size.height, 10 ) / 100 * size.presentationHeight;
        }

        return size;

    }

    /**
     * Applies a CSS transform to the target element.
     */
    function transformElement( element, transform ) {

        element.style.WebkitTransform = transform;
        element.style.MozTransform = transform;
        element.style.msTransform = transform;
        element.style.transform = transform;

    }

    /**
     * Applies CSS transforms to the slides container. The container
     * is transformed from two separate sources: layout and the overview
     * mode.
     */
    function transformSlides( transforms ) {

        // Pick up new transforms from arguments
        if( typeof transforms.layout === 'string' ) slidesTransform.layout = transforms.layout;
        if( typeof transforms.overview === 'string' ) slidesTransform.overview = transforms.overview;

        // Apply the transforms to the slides container
        if( slidesTransform.layout ) {
            transformElement( dom.slides, slidesTransform.layout + ' ' + slidesTransform.overview );
        }
        else {
            transformElement( dom.slides, slidesTransform.overview );
        }

    }


    /**
     * Finds and stores references to DOM elements which are
     * required by the presentation. If a required element is
     * not found, it is created.
     */
    function setupDOM() {

        dom.helper = document.querySelector('.helper');
        dom.timerDiv = document.querySelector('.timer');

        dom.closeBtns = toArray(document.querySelectorAll('ins.close'));
        dom.popups = toArray(document.querySelectorAll('.popup'));

        dom.start = document.querySelector('.play-btn');
        dom.hearts = document.querySelectorAll('.controls tr td')[0];
        dom.timer = document.querySelector(TIMER_DIV);
        dom.points = document.querySelectorAll('.controls tr td')[2];

        dom.questions = toArray(document.querySelectorAll(SECTION_QUESTION_SELECTOR));
        dom.button  = toArray(document.querySelectorAll(NAVIGATION_SELECTOR));

        dom.stats = document.querySelector('#finish .stats-table');

        dom.restartBtn = document.querySelector('.btn-restart');

        var elem;
        dom.questions.forEach(function(question) {
            elem = createSingletonNode(dom.slides, 'div', 'animation_' + question.dataset.question, null);
            elem.classList.add('animation');
        });

        // Draw animation divs for slides with id and main slide
        elem = [
            createSingletonNode(dom.slides, 'div', 'animation_0' , null),
            createSingletonNode(dom.slides, 'div', 'animation_join' , null),
            createSingletonNode(dom.slides, 'div', 'animation_level' , null),
            createSingletonNode(dom.slides, 'div', 'animation_game-over' , null)
        ];
        elem.forEach(function(el){el.classList.add('animation');});

        // Level buttons for section#level
        for (var index in difficulty) {
            createSingletonNode(dom.wrapper.querySelector('#level'), 'button', index, null);
        }

        dom.levelButtons = toArray(document.querySelectorAll('#level button'));

        dom.pseudoInputs = toArray(document.querySelectorAll('form div.text div'));

    }

    function setupControls() {

        var hearts = '',
            points = '';
        //draw hearts and pointers
        // Element containing notes that are visible to the audience
        for (var i = 1; i <= config.lives; i++) {
            hearts += '<a href="#" class="heart"></a>';
        }

        for (var j = 1; j <= questionCount; j++) {
            points += '<a href="#/' + j + '" class="question_point question_' + j + '"></a>';
        }

        createSingletonNode( dom.points, 'div', 'status', points );
        createSingletonNode( dom.points, 'ins', 'finish', null );
        createSingletonNode( dom.hearts, 'div', 'hearts', hearts );

        dom.pointArray =  toArray(dom.points.querySelectorAll('a'));
        dom.heartArray =  toArray(dom.hearts.querySelectorAll('a'));
        document.querySelector('.status').classList.add(level);

        livesCount = dom.heartArray.length;

        // hide helper popup if it was shown
        dom.helper.nextElementSibling.setAttribute('hidden', '');
        dom.helper.nextElementSibling.setAttribute('arai-hidden', 'true');

        showControlsBar();
    }


    /**
     * shuffle questions and slice to the count of questions
     */
    function setupQuestions() {

        if (config.shuffle) {
            dom.questions = shuffle();
        }

        dom.questions = dom.questions.slice(0, questionCount);

    }

    /**
     * Randomly shuffles all questions.
     */
    function shuffle() {
        var array = toArray(document.querySelectorAll(SECTION_QUESTION_SELECTOR)),
            currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    /**
     * Creates an HTML element and returns a reference to it.
     * If the element already exists the existing instance will
     * be returned.
     */
    function createSingletonNode( container, tagname, classname, innerHTML ) {

        // Find all nodes matching the description
        var nodes = container.querySelectorAll( '.' + classname );

        // Check all matches to find one which is a direct child of
        // the specified container. If matches, delete child element
        for( var i = 0; i < nodes.length; i++ ) {
            var testNode = nodes[i];
            if( testNode.parentNode === container ) {
                container.removeChild(testNode);
            }
        }

        // If no node was found, create it now
        var node = document.createElement( tagname );
        node.classList.add( classname );
        if( typeof innerHTML === 'string' ) {
            node.innerHTML = innerHTML;
        }
        container.appendChild( node );

        return node;

    }

    /**
     * Creates a animation for the given slide.
     */
    function createAnimation() {

        var index;

        if (typeof indexh === 'number' && indexh != 0) {
            var question = dom.questions[questionIndex];
            index = question.dataset.question;
        } else {
            index = indexh;
        }

        dom.slides.dataset.id = index;

        var compositionName = 'animation_' + index,
            filePath = 'javascripts/animation/' + compositionName,
            element = document.getElementsByClassName(compositionName);

        if (element.length) {
            if (window.AdobeEdge.getComposition(compositionName) == undefined) {

                window.AdobeEdge.loadComposition(filePath, compositionName, {
                    scaleToFit: "none",
                    centerStage: "none",
                    minW: "0px",
                    maxW: "1920px",
                    width: "1920px",
                    height: "1080px"
                }, {dom: [ ]}, {
                    dom: [

                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: true,
                            rect: [undefined, undefined, '1920px', '1080px'],
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                });
            } else if (window.AdobeEdge.getComposition(compositionName).getStage() != undefined) {
                window.AdobeEdge.getComposition(compositionName).getStage().playAll();
            }
        }
    }

    /**
     * Reads the current URL (hash) and navigates accordingly.
     */
    function readURL() {

        var hash = window.location.hash;

        if (slidesStarted && hash == '#/') {
            window.location.reload();
            return;
        }

        // Attempt to parse the hash as either an index or name
        var bits = hash.slice( 2 ).split( '/' ),
            name = hash.replace( /#|\//gi, '').replace(/[0-9]/gi, '');

        dom.slides.classList.remove('question');

        // If slides not started go to the first slide
        if (!slidesStarted && name != 'finish') {
            slide(0);
        }
        else if (gameOverFlag) {
            slide('game-over');
        }
        // If the first bit is invalid and there is a name we can
        // assume that this is a named link
        else if ( isNaN( parseInt( bits[0], 10 ) ) && name.length ) {
            var element;

            // Ensure the named link is a valid HTML ID attribute
            if ( /^[a-zA-Z][\w:.-]*$/.test( name ) ) {
                // Find the slide with the specified ID
                element = document.getElementById( name );
            }

            if (bits[1] && bits[1] > 0)
                indexUser = bits[1];

            if ( element ) {

                if (name == 'finish')
                    getStats(setupStats);

                slide(name);
            }
            // If the slide doesn't exist, navigate to the current slide
            else {

                slide( indexh || 0);
            }

        }
        else {

            if (!gameStarted)
                slide(0);

            // Read the index components of the hash
            // numeric value is only for questions
            var h = parseInt( bits[0], 10 ) || 0;

            dom.slides.classList.add('question');

            if( h !== indexh ) {
                slide( h );
            }
        }

    }

    /**
     * Updates the page URL (hash) to reflect the current
     * state.
     */
    function writeURL() {

        if (currentSlide) {
            var url = '/';

            // Attempt to create a named link based on the slide's ID
            var id = currentSlide.getAttribute( 'id' );
            if (id) {
                id = id.replace( /[^a-zA-Z0-9\-\_\:\.]/g, '' );
            }

            // If the current slide has an ID, use that as a named link
            if (typeof id === 'string' && id.length) {
                url = '/' + id;
                if (indexUser > 0) url += '/' + indexUser
            }
            // Otherwise use the /h index
            else {

                if( indexh > 0 ) url += indexh;
            }

            window.location.hash = url;
        }

    }

    /**
     * Steps from the current point in the presentation to the
     * slide which matches the specified horizontal and vertical
     * indices.
     *
     * @param {(int|string)} h hash of the slide
     * Horizontal index of the target slide
     * target slide to activate
     */
    function slide(h) {

        // Activate and transition to the new slide
        indexh = updateSlides( h === undefined ? indexh : h );
        createAnimation();

        // Store references to the previous and current slides
        currentSlide = dom.wrapper.querySelector('section.present');

        writeURL();

    }

    /**
     * Updates one dimension of slides by showing the slide
     * with the specified index.
     *
     * @param {(String|Number)} index The index of the slide that should be
     * shown
     *
     * @return {Number} The index of the slide that is now shown,
     * might differ from the passed in index if it was out of
     * bounds.
     */
    function updateSlides( index ) {

        // Select all slides and convert the NodeList result to
        // an array
        var slides = toArray(document.querySelectorAll(SECTION_SELECTOR)),
            slidesLength = slides.length,
            animationWrapper = toArray(document.body.querySelectorAll('.animation')),
            animationIndex = index;


        for( var i = 0; i < slidesLength; i++ ) {
            var element = slides[i];

            element.classList.remove( 'present' );
            // http://www.w3.org/html/wg/drafts/html/master/editing.html#the-hidden-attribute
            element.setAttribute( 'hidden', '' );
            element.setAttribute( 'aria-hidden', 'true' );
        }

        // if it is main page
        if (index == 0) {
            slides[index].classList.add( 'present' );
            slides[index].removeAttribute( 'hidden' );
            slides[index].removeAttribute( 'aria-hidden' );
        }
        // if it is page with id
        else if (typeof index === 'string') {
            var el;

            timerStop();
            hideControlsBar();

            // Ensure the named link is a valid HTML ID attribute
            if( /^[a-zA-Z][\w:.-]*$/.test( index ) ) {
                // Find the slide with the specified ID
                el = document.getElementById( index );
            }

            if( el ) {
                // Find the position of the named slide and navigate to it
                var indices = getIndices(el);

                slides[indices.h].classList.add( 'present' );
                slides[indices.h].removeAttribute( 'hidden' );
                slides[indices.h].removeAttribute( 'aria-hidden' );
                slides[indices.h].dataset.user = indexUser;

            }
            else {
                slides[0].classList.add( 'present' );
                slides[0].removeAttribute( 'hidden' );
                slides[0].removeAttribute( 'aria-hidden' );
            }

        }
        // if it is question
        else if (typeof index === 'number') {

            questionIndex = index - 1;

            dom.questions[questionIndex].classList.add( 'present' );
            dom.questions[questionIndex].removeAttribute( 'hidden' );
            dom.questions[questionIndex].removeAttribute( 'aria-hidden' );

            animationIndex = getCurrentQuestionId();

            setActivePoint();

            // check if slide has been visited
            checkSlide();

        } else {
            // show main page
            slides[0].classList.add( 'present' );
            slides[0].removeAttribute( 'hidden' );
            slides[0].removeAttribute( 'aria-hidden' );
        }


        // Hide all animation
        if (animationWrapper.length) {
            for (var j = 0; j < animationWrapper.length; j++) {
                if (animationWrapper[j].classList.contains('animation_' + animationIndex)) {
                    animationWrapper[j].removeAttribute('hidden');
                    animationWrapper[j].removeAttribute('aria-hidden');
                } else {
                    animationWrapper[j].setAttribute('hidden', '');
                    animationWrapper[j].setAttribute('aria-hidden', 'true');
                }
            }
        }

        return index;

    }

    /**
     * Checks if this slide war visited and has the answer
     */
    function checkSlide() {

        var currentQuestion = getCurrentQuestionId();

        if (currentQuestion) {
            for (var i = 0; i < state.length; i++) {
                if (state[i].questionID == currentQuestion) {

                    var answer = state[i].result ? 'right' : 'wrong';
                    dom.pointArray[questionIndex].classList.add(answer);
                    dom.pointArray[questionIndex].classList.remove('active');

                    // if there is no questions with no answer, go to finish
                    if (!missedQuestion()) {
                        slide("finish");
                    }
                }
            }
        }

    }

    function addSocketListener() {
        socket
            .on('connect', function() {
                //console.info('connected');
            })
            .on('disconnect', function() {
                //console.info('disconnected');
            })
            .on('response:correct answer?', function(data) {

                state.push({
                    position: indexh,
                    questionID : data.questionId,
                    answer : data.answerId,
                    result : data.result
                });

                toggleAnswer(data.answerId, data.result);

                togglePoint(data.result);

                navigateRight();

            });
    }


    function addEventListeners() {

        window.addEventListener( 'hashchange', onWindowHashChange, false );
        window.addEventListener('resize', onWindowResize, false);
        dom.stats.addEventListener('scroll', onStatsScroll, false);

        dom.start.addEventListener( 'click', startSlides, false );

        dom.levelButtons.forEach(function(el) {el.addEventListener( 'click', setGameLevel, false );});
        dom.button.forEach(function(el) {el.addEventListener( 'click', navigation, false );});

        dom.restartBtn.addEventListener('click', function() { gameOverFlag = false; slide('level');}, false);

        dom.helper.addEventListener('click', showPopup, false);
        dom.closeBtns.forEach(function(el) {el.addEventListener('click', hidePopup, false);});

        dom.timerDiv.addEventListener('click', showPopup, false);

        dom.pseudoInputs.forEach(function(el) {
            el.addEventListener('keyup', keyUpDiv, false);
            el.addEventListener('focus', focusDiv, false);
        });
    }

    /**
     * Unbinds all event listeners.
     */
    function removeEventListeners() {

        window.removeEventListener( 'hashchange', onWindowHashChange, false );
        window.removeEventListener('resize', onWindowResize, false);
        dom.stats.removeEventListener('scroll', onStatsScroll, false);

        dom.start.removeEventListener( 'click', startSlides, false );

        dom.levelButtons.forEach(function(el) {el.removeEventListener( 'click', setGameLevel, false );});
        dom.button.forEach(function(el) {el.removeEventListener( 'click', navigation, false );});

        dom.restartBtn.removeEventListener('click', function() { gameOverFlag = false; slide('level');}, false);

        dom.helper.removeEventListener('click', showPopup, false);
        dom.closeBtns.forEach(function(el) {el.removeEventListener('click', hidePopup, false);});

        dom.timerDiv.removeEventListener('click', showPopup, false);

        dom.pseudoInputs.forEach(function(el) {
            el.removeEventListener('keyup', keyUpDiv, false);
            el.removeEventListener('focus', focusDiv, false);
        });

    }

    /**
     * Sets up answers to default
     * by removing classes right/wrong
     */
    function setupAnswers() {
        dom.button.forEach(function(el) {

            var ins = el.querySelector('ins'),
                p = el.querySelector('p');

            ins.classList.remove('right');
            ins.classList.remove('wrong');

            p.classList.remove('right');
            p.classList.remove('wrong');
        });
    }


    function setActivePoint() {

        dom.pointArray.forEach(function(el) { el.classList.toggle('active', false); });
        dom.pointArray[questionIndex].classList.toggle('active');
    }

    function showControlsBar() {
        dom.controls.removeAttribute( 'hidden');
        dom.controls.removeAttribute( 'aria-hidden');
    }

    function hideControlsBar() {
        dom.controls.setAttribute( 'hidden', '' );
        dom.controls.setAttribute( 'aria-hidden', 'true' );
    }

    function timerStart(time) {

        state.timeStart = new Date;

        clearTimeout(timerID);
        timerID = setInterval(
            function() {
                setTime(time);
                if (time == 0) {
                    gameOver();
                }
                time--;
            },
            1000
        )
    }

    function timerStop() {
        clearInterval(timerID);
    }

    function setTime(time) {

        var min = Math.floor(time / 60),
            sec = time - min * 60;

        min = min < 10 ? '0' + min : min;
        sec = sec < 10 ? '0' + sec : sec;

        dom.timer.innerHTML = min + ':' + sec;
        if (time < 60*5) {
            dom.timer.parentNode.classList.add('out-of-time');
            dom.timer.parentNode.previousElementSibling.classList.add('few-time');
            if (!fewTimeFlag) {
                dispatchEvent(dom.timerDiv, 'click');
                fewTimeFlag = true;
            }
        }

    }

    function gameOver() {

        timerStop();
        gameOverFlag = true;
        slide('game-over');

        return false;

    }

    /**
     * Simulating event to element
     * @param {HTMLElement} el - element we need to
     * @param {String} etype - event name
     */
    function dispatchEvent(el, etype) {
        if (el.fireEvent) {
            el.fireEvent('on' + etype);
        } else {
            var evObj = document.createEvent('Events');
            evObj.initEvent(etype, true, false);
            el.dispatchEvent(evObj);
        }
    }

    /**
     * Retrieves the h location of the current, or specified,
     * slide.
     *
     * @param {HTMLElement} slide If specified, the returned
     * index will be for this slide rather than the currently
     * active one
     *
     * @return {Object} { h: <int>}
     */
    function getIndices( slide ) {

        // By default, return the current indices
        var h = indexh;

        // If a slide is specified, return the indices of that slide
        if( slide ) {
            // Select all slides
            var Slides = dom.sections;

            // Now that we know which the slide is, get its index
            h = Math.max( Slides.indexOf( slide ), 0 );
        }

        return { h: h };

    }

    /**
     * Returns index of the current question
     * @returns {number}
     */
    function getCurrentQuestionId() {

        return dom.questions[questionIndex].dataset.question;
    }

    /**
     * Determine what available routes there are for navigation.
     *
     * @return {(int|Boolean)}
     */
    function missedQuestion() {
        var questions = [];
        for (var i = 0; i < state.length; i++) {
            questions.push(state[i].position);
        }

        for (var index = 1; index <= questionCount; index++) {
            if (questions.indexOf(index) == -1) {
                return index;
            }
        }

        return state.length < questionCount ? indexh : false;

    }


    function extend( a, b ) {

        for (var i in b) {
            a[i] = b[i];
        }

    }

    /**
     * Converts the target object to an array.
     */
    function toArray( o ) {

        return Array.prototype.slice.call( o );

    }

    /**
     * Toggles the equal point to status right or wrong.
     *
     * @param {Boolean} answer
     */
    function togglePoint(answer) {

        answer ? setRightPoint() : setWrongPoint();

    }

    function setRightPoint() {

        dom.pointArray[questionIndex].classList.add('right');
    }

    function setWrongPoint() {

        dom.pointArray[questionIndex].classList.add('wrong');
        killLife();
    }

    /**
     * Toggles the equal answer to status right or wrong.
     *
     * @param {int} answerId current clicked answer id
     * @param {Boolean} isRight
     */
    function toggleAnswer(answerId, isRight) {

        var ins = document.querySelector('[data-answer="' + answerId + '"] ins');

        isRight ? setRightAnswer(ins) : setWrongAnswer(ins);

    }

    function setRightAnswer(ins) {

        ins.classList.add('right');
        ins.nextElementSibling.classList.add('right');
    }

    function setWrongAnswer(ins) {

        ins.classList.add('wrong');
        ins.nextElementSibling.classList.add('wrong');

        dispatchEvent(dom.helper, 'click');

    }

    function killLife() {
        livesCount--;

        document.querySelector('.hearts').removeChild( dom.heartArray[livesCount] );

        if (livesCount == 0) {
            setTimeout(function() {
                gameOver();
                return false;
            }, 1);

        }
    }

    /**
     * Added info about spent time, count of right questions
     * and level of the game to database
     * Updates all the time when game was restarted or if it was finished successfully
     */
    function statsState(callback) {
        var rightAnswers = 0;

        for (var i = 0; i < state.length; ++i) {
            if (state[i].result) ++rightAnswers;
        }

        $.ajax('/stat', {
            method: 'POST',
            data: {
                levelName: level,
                rightAnswers: rightAnswers,
                totalTime: (new Date() - state.timeStart) / 1000,
            },

            success: function(data,textStatus, jqXHR) {

                callback(null, 'data')
            },
            error: function(jqXHR, textStatus, errorThrown) {

                callback(errorThrown)
            }
        });

    }


    function getStats(callback) {

        var id = indexUser || '';

        $.ajax('/stat/user/' + id, {
            method: 'GET',
            data: {
                include : 'users'
            },

            success: function(data,textStatus, jqXHR) {

                callback(null, data)
            },
            error: function(jqXHR, textStatus, errorThrown) {

                callback(jqXHR)
            }
        });
    }

    function getAllStats(callback) {

        $.ajax('/stat', {
            method: 'GET',
            data: {
                include : 'users'
            },

            success: function(data,textStatus, jqXHR) {

                callback(null, data, "next")
            },
            error: function(jqXHR, textStatus, errorThrown) {

                callback(jqXHR)
            }
        });
    }

    function setupStats(err, data, dest) {

        var i;

        if (err) {

            if (err.status == 404)
                getAllStats(setupStats);

        } else {

            if (!data.length) {
                dest == "next" ? noMoreNextStat = true : noMorePrevStat = true;
            }
            // if we have prev and next elements
            else if (Array.isArray(data[1]) || Array.isArray(data[2])) {

                if (indexUser) {
                    var ol = dom.stats.querySelector('ol');
                    while (ol.firstChild)
                        ol.removeChild(ol.firstChild);

                    noMoreNextStat = false; noMorePrevStat = false;
                }

                indexUser = topStatId = bottomStatId = data[0].id;
                topRating = bottomRating = data[0].rating;
                // Create active element
                createStatsRow(data[0], 'next');

                // Create elements before active
                for (i = data[1].length - 1; i >= 0; i--) {
                    createStatsRow(data[1][i], 'prev');
                    topStatId = data[1][i].id;
                    topRating = data[1][i].rating;
                }

                // Create elements after active
                for (i = 0; i < data[2].length; i++) {
                    createStatsRow(data[2][i], 'next');
                    bottomStatId = data[2][i].id;
                    bottomRating = data[2][i].rating;
                }

                centerActiveUser();

            } else if (data.length) {

                topStatId = data[0].id;
                topRating = data[0].rating;

                bottomStatId = data[data.length - 1].id;
                bottomRating = data[data.length - 1].rating;

                switch (dest) {
                    case "next" :

                        for (i = 0; i < data.length; i++) {
                            createStatsRow(data[i], dest);
                        }

                        break;
                    case "prev" :

                        for (i = data.length -1 ; i >= 0 ; i--) {
                            createStatsRow(data[i], dest);
                        }

                        break;
                }
            }

            createCopyLink();
        }


    }

    function createStatsRow(obj, dest) {

        var node, position, name, rating;

        node = document.createElement('li');

        if (obj.id == indexUser)
            node.classList.add('active');

        position = document.createElement('ins');
        position.innerHTML = obj.id;

        name = document.createElement('span');
        name.classList.add('name');
        name.innerHTML = obj.name;

        rating = document.createElement('span');
        rating.innerHTML = obj.rating;

        node.appendChild(position);
        node.appendChild(name);
        node.appendChild(rating);

        if (dest == "next")
            dom.stats.querySelector('ol').appendChild(node);
        else if (dest == "prev") {
            var ol = document.querySelector('ol');
            // prepend our span element to our section element
            ol.insertBefore(node, ol.firstChild);
        }

    }

    function centerActiveUser() {

        var el = document.querySelector('li.active'),
            elOffset = el.offsetTop,
            elHeight = el.offsetHeight,
            olOffset = document.querySelector('ol').offsetTop,
            offset;

        offset = (elOffset - olOffset) - (elHeight * 3);

        dom.stats.scrollTop = offset;
    }

    function createCopyLink() {
        var link = document.querySelector('a.link');

        if (indexUser)
            link.dataset.clipboardText = window.location.href.indexOf('/' + indexUser) != -1
                ? window.location.href
                : window.location.href + '/' + indexUser;
        else
            link.dataset.clipboardText = window.location.href;

        new Clipboard('.link')
            .on('success', function (e) {
                e.clearSelection();
            })
            .on('error', function (e) {
                link.innerHTML = link.dataset.clipboardText;
                link.classList.remove('link');
            });
    }

    /*----------------------------------------------------------*/
    /*-----------------------   EVENTS   -----------------------*/
    /*----------------------------------------------------------*/

    function startSlides() {
        slidesStarted = true;

        if (document.getElementById('join'))
            slide('join');
        else
            slide('level');
    }

    /**
     * Form div on focus event
     */
    function focusDiv() {

        if (this.parentElement.id == 'address')
            document.querySelector('.help-message').style.display = 'block';
    }

    /**
     * On key up write inner text to input
     */
    function keyUpDiv() {

        var inputName = this.parentElement.id,
            text = this.innerText;

        if (text.replace(/\s+/g, " ") > '') {
            document.querySelector('form input[name="' + inputName + '"]').value = text;
        } else {
                this.innerHTML = "";
        }
    }

    function navigateRight() {

        var nextQuestion = missedQuestion();

        // if some questions has been missed go to the min missed question
        if (nextQuestion) {
            clearTimeout(nextSlideTimeout);

            nextSlideTimeout = setTimeout(function() {
                    slide(nextQuestion);
                }, 1000
            );
        }
        else {
            // go to finish slide
            finishGame();
        }

    }

    /**
     * Handler for the window level 'hashchange' event.
     */
    function onWindowHashChange() {

        readURL();

    }

    /**
     * Handler for the window level 'resize' event.
     */
    function onWindowResize() {

        layout();

    }

    function onStatsScroll() {

        var ol = this.querySelector('ol'),
            dir, id, rating;

        if (ol.offsetHeight - this.offsetHeight == this.scrollTop) {
            // scroll down
            if (noMoreNextStat)
                return false;

            dir = 'next';
            id = bottomStatId;
            rating = bottomRating;

        } else if (this.scrollTop == 0) {
            // scroll up

            if (noMorePrevStat)
                return false;

            dir = 'prev';
            id = topStatId;
            rating = topRating;

        } else {
            return false;
        }

        $.ajax('/stat', {
            method: 'GET',
            data: {
                dir : dir,
                id : id,
                rating : rating
            },

            success: function(data,textStatus, jqXHR) {

                setupStats(null, data, dir);
            },
            error: function(jqXHR, textStatus, errorThrown) {

                setupStats(errorThrown);
            }
        });
    }

    function showPopup() {

        this.classList.add('active');
        this.nextElementSibling.removeAttribute('hidden');
        this.nextElementSibling.removeAttribute('aria-hidden');
    }

    function hidePopup() {

        this.parentElement.previousElementSibling.classList.remove('active');
        this.parentElement.setAttribute('hidden', '');
        this.parentElement.setAttribute('aria-hidden', 'true');

    }

    function navigation() {

        var answerId = this.dataset.answer,
            currentQuestion = getCurrentQuestionId(),
            currentSlideAnswers = toArray(dom.questions[questionIndex].querySelectorAll('li')),
            answer;

        // remove click listener for current question to prevent answering again
        currentSlideAnswers.forEach(function(el) { el.removeEventListener('click', navigation, false); });

        socket.emit('request:correct answer?', {
            answerId: answerId,
            questionId: currentQuestion
        });

    }

    function setGameLevel() {

        level = this.className;

        questionCount = difficulty[level];

        startGame();

    }

    /**
     * Handler on play-btn click, starting the game
     */
    function startGame() {

        gameStarted = true;

        // sendStats( statsState(), true );

        clearInterval(nextSlideTimeout);

        state = [];
        state.length = 0;

        // sets up answers to default
        setupAnswers();

        removeEventListeners();
        addEventListeners();

        setupControls();
        timerStart(config.gameTime, setTime);

        // set question array, shuffle it and slice
        setupQuestions();

        // go to first question
        slide(1);
    }

    /**
     * show finish slide
     */
    function finishGame() {
        statsState(function(err, data) {
            if (err) {
                // Bad
            } else {
                slide('finish');
            }
        })
    }

    Game = {
        initialize: initialize
    };

    return Game;
}));