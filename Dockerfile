FROM node
MAINTAINER Kirill Lyubaev <k.lyubaev@404-group.com>

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN npm i -g \
    bower \
    gulp \
    nodemon \
    sequelize-cli

COPY package.json /usr/src/app/
RUN npm i

COPY . /usr/src/app

EXPOSE 3000 5858

# https://github.com/remy/nodemon/issues/419#issuecomment-143389532
CMD ["nodemon", "-L"]
