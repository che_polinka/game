var async = require('async');
var express = require('express');
var router = express.Router();

var models = require('../models');
var User = models.user;
var Stat = models.stat;
var Level = models.level;

var auth = function(req, res, next) {
  if (! req.session.userid) {
    err = new Error('Unauthorized');
    err.status = 401;

    return next(err);
  }

  return next();
};
/**
 * Получить текущую статистику.
 *
 * GET /stat
 *
 * Query Parameters:
 *   - id      (int)    : id пользователя, относительно которго нужно запросить
 *                        статистику.
 *   - rating  (int)    : рейтинг пользователя, относительно которго нужно
 *                        запросить статистику.
 *   - dir     (string) : направление, в котором будет получена статистика,
 *                        относительно переданных id и rating параметров.
 *                        (default: "next")
 */
router.get('/', function(req, res, next) {
  User.findByDirection(req.query.dir || 'next', req.query.id, req.query.rating, function(err, users) {
    if (err) {
      return next(err)
    }

    return res.send(users);
  })
});

/**
 * Получить статистику пользователя.
 *
 * GET /stat/user/:id
 *   :id - ID пользователя (default: session.userid)
 *
 * Query Parameters:
 *   - include (string) : включить предыдущих и следующих пользователей
 *                        (@see scope)
 */
router.get('/user/:id?', function(req, res, next) {
  var userId = parseInt(req.params.id, 10) || req.session.userid;
  var isIncludeUsers = ('users' === req.query.include);

  if (! userId)
    return res.status(404).send({ message: 'Not Found' });

  async.waterfall([
    /**
     * Find user by id.
     */
    function(callback) {
      User.findById(userId, {limit: 1})
        .then(function(user) {
          if (user == null) {
            err = new Error('Not Found')
            err.status = 404;

            callback(err)
          } else {
            callback(null, user)
          }
        })
        .catch(callback)
    },

    function(user, callback) {
      if (isIncludeUsers) {
        async.parallel({

          /**
           * Find previous users. Limit (see scope)
           */
          prev: function(callback) {
            User.findPrev(user.id, user.rating, function(err, users) {
              if (err) callback(err)
              else callback(null, users)
            });
          },

          /**
           * Find next users. Limit (see scope)
           */
          next: function(callback) {
            User.findNext(user.id, user.rating, function(err, users) {
              if (err) callback(err)
              else callback(null, users)
            });
          },

        }, function(err, users) {
          if (err) callback(err)
          else callback(null, user, users.prev, users.next)
        });

      } else {
        callback(null, user)
      }
    },

  ],
  function(err, users, prevUsers, nextUsers) {
    if (err) {
      return next(err)
    }

    return res.send([].slice.call(arguments, 1));
  });
});

/**
 * Сохранить статистику пользователя.
 *
 * curl -D - -XPOST --data "levelName=hard" $(docker-machine ip):8080/stat
 */
router.post('/', [ auth ], function(req, res, next) {
  async.waterfall([
    /**
     * Find level id by name.
     */
    function(callback) {
      Level.findOne( { where: { name: req.body.levelName } } )
        .then(function(level) {
          if (level == null) {
            callback(new Error('Level not found'));
          } else {
            callback(null, level.id)
          }
        })
        .catch(callback)
    },

    /**
     * Create new stat for current user.
     */
    function (levelId, callback) {
      Stat.create({
          user_id: req.session.userid,
          level_id: levelId,
          num_of_answers: parseInt(req.body.rightAnswers, 10) || 0,
          total_time: parseInt(req.body.totalTime, 10) || 0,
        })
        .then(function(stat) {
          callback(null, stat.id)
        })
        .catch(callback)
    },

  ], function(err, statId) {
    if (err) {
      return next(err);
    } else {
      res.send({'created': statId})
    }
  });

});

module.exports = router;
