var async = require('async');
var csrf = require('csurf');
var shuffle = require('shuffle-array');
var express = require('express');
var router = express.Router();

var models = require('../models');
var User = models.user;
var Answer = models.answer;
var Question = models.question;


router.get('/', csrf(), function(req, res, next) {

  async.parallel([
    /**
     * Find all the questions and related the answers.
     */
    function(callback) {
      Question.findAll( { include: [ Answer ] } )
        .then(function(questions) {

          // Randomize the order of the elements in a given array.
          questions.forEach(function(row) {
            shuffle(row.answers)
          })

          callback(null, questions)

        }).catch(callback);
    },

  ], function(err, result) {
    if (err) {
      return next(err)
    }

    res.render('index', {
      title: 'Application',

      questions: result[0],

      csrfToken: req.csrfToken(),
      isAuth: req.session.authenticated,
    });

  });
});


router.post('/login', csrf(), function(req, res, next) {
    User.auth(req.body.email, req.body.phone, req.body.name, req.body.address, function(err, user, created) {
        if (err) {
            if (err.status !== 403) {
                return next(err);
            }

            if (req.xhr) {
              return res.status(err.status).send(err.errors);
            }

            req.flash('errors', err.errors);
        } else {
            req.session.userid = user.id;
            req.session.authenticated = true;

            if (req.xhr) {
                return res.status(created && 201 || 200).end();
            }
        }

        res.redirect('back');
    });
});


router.get('/logout', function(req, res) {
    req.session = null;
    res.redirect('back');
});

module.exports = router;
