'use strict';

module.exports = function(sequelize, DataTypes) {
  var user = sequelize.define('user', {
    email: {
      type: DataTypes.STRING,
      validate: { isEmail: { msg: 'Введите правильный email адрес' } }
    },

    phone: {
      type: DataTypes.STRING,
      // https://github.com/chriso/validator.js/blob/master/validator.js#L886
      validate: { isMobilePhone: { args: ['ru-RU'], msg: 'Номер телефона не соответствует формату +79ХХХХХХХХХ' } }
    },

    name: {
      type: DataTypes.STRING,
      validate: { len: {args: [2, 255], msg: 'Имя некорректно' } }
    },

    address: {
      type: DataTypes.STRING,
      validate: { len: {args: [2, 255], msg: 'Адрес некорректный' } }
    },

    rating: DataTypes.INTEGER

  }, {
    classMethods: {
      associate: function(models) {
        //
      }
    },

    defaultScope: { limit: 20 },

    scopes: {

      andWithRating: {
        where: {
          $and: {
            rating: { $gt: 0 }
          }
        }
      },

    },

    validate: {
      notAnonymous: function() {
        if (this.email == null && this.name == null
          && this.phone == null && this.address == null
        ) {
          throw new Error('Все поля обязательны для заполнения')
        }
      }
    },

    underscored: true
  });

  /**
   * Registration and/or authorization new user. Static model method.
   *
   * @param  {string}   email
   * @param  {string}   phone
   * @param  {string}   name
   * @param  {string}   address
   * @param  {Function} callback
   * @return {null}
   */
  user.auth = function(email, phone, name, address, callback) {
    var User = this;

    User
      .findOrCreate({ where: { email: email, phone: phone }, defaults: { name: name, address: address } })
      .spread(function(user, created) {
        callback(null, user, created);
      })
      .catch(function(err) {
        if (err instanceof sequelize.ValidationError) {
          err.status = 403;
        }

        callback(err);
      })
  };

  user.findByDirection = function(direction, id, rating, callback) {
    var User = this;

    var
      id = parseInt(id, 10) || 0
    , rating = parseInt(rating, 10) || 0

    , applyReverse
    , options = { order: [ ['rating', 'DESC'], ['id', 'DESC'] ] }
    ;

    if (id > 0) {
      switch(direction.toLowerCase()) {
        case 'next':
          applyReverse = false;

          options.where = {
            rating: { $lte: rating },
            $or: [
              { id: { $lt: id } },
              { rating: { $lt: rating } }
            ]
          }

          break

        case 'prev':
          applyReverse = true;

          options.where = {
            rating: { $gte: rating },
            $or: [
              { id: { $gt: id } },
              { rating: { $gt: rating } }
            ]
          }

          options.order = [ ['rating', 'ASC'], ['id', 'ASC'] ];
          break

        default:
          return callback(new Error('Unknown direction'))
      }
    }

    User
      .scope('defaultScope', 'andWithRating')
      .findAll(options)
      .then(function(users) {
        // после ASC сортировки (для получения ближайших записей),
        // сортируем в обратном порядке результирующий массив.
        applyReverse && users.reverse()
        callback(null, users)
      })
      .catch(callback);
  };

  user.findNext = function(id, rating, callback) {
    var User = this;

    User.findByDirection('next', id, rating, callback);
  };

  user.findPrev = function(id, rating, callback) {
    var User = this;

    User.findByDirection('prev', id, rating, callback);
  };

  return user;
};
