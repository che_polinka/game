'use strict';
module.exports = function(sequelize, DataTypes) {
  var stat = sequelize.define('stat', {
    user_id: DataTypes.INTEGER,
    level_id: DataTypes.INTEGER,
    num_of_answers: DataTypes.INTEGER,
    total_time: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },

    underscored: true
  });
  return stat;
};
