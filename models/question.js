'use strict';
module.exports = function(sequelize, DataTypes) {
  var question = sequelize.define('question', {
    text: DataTypes.STRING,
    tip: DataTypes.TEXT,
    trusted_answer_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        question.hasMany(models.answer);
        question.belongsTo(models.answer, { as: 'TrustedAnswer', constraints: false })
      }
    },

    timestamps: false,
    underscored: true
  });
  return question;
};
