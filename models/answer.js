'use strict';
module.exports = function(sequelize, DataTypes) {
  var answer = sequelize.define('answer', {
    text: DataTypes.STRING,
    question_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },

    timestamps: false,
    underscored: true
  });
  return answer;
};
