'use strict';
module.exports = function(sequelize, DataTypes) {
  var level = sequelize.define('level', {
    name: DataTypes.STRING,
    rate: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },

    timestamps: false,
    underscored: true
  });
  return level;
};
