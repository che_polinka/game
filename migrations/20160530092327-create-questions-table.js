'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('questions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      text: {
        allowNull: false,
        type: Sequelize.STRING
      },
      tip: {
        type: Sequelize.TEXT
      }
    }, {charset: 'utf8'});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('questions');
  }
};
