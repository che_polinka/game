'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('answers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      text: {
        allowNull: false,
        type: Sequelize.STRING
      },
      question_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'questions',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      }
    }, {charset: 'utf8'});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('answers');
  }
};
