'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var trigger = [];

    trigger.push("CREATE TRIGGER `calculate_the_rating` AFTER INSERT ON `stats`");
    trigger.push("FOR EACH ROW BEGIN");
    trigger.push("  UPDATE `users` SET `rating` = ROUND( ( (NEW.num_of_answers * 100 * 1000) / NEW.total_time ) )");
    trigger.push("    WHERE `id` = NEW.user_id;");
    trigger.push("END;");

    return queryInterface.sequelize.query(trigger.join(' '));
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('DROP TRIGGER IF EXISTS `calculate_the_rating`;');
  }
};


