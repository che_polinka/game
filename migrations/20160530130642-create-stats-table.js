'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('stats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'users',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      level_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'levels',
            key: 'id'
        },
        onUpdate: 'cascade'
      },
      num_of_answers: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0,
        comment: 'количество правильных ответов',
      },
      total_time: {
        allowNull: false,
        type: Sequelize.INTEGER,
        comment: 'общее затраченное время в секундах',
      },
      created_at: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {charset: 'utf8'});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('stats');
  }
};
