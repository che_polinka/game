'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn('questions', 'trusted_answer_id', {
        type: Sequelize.INTEGER,
        references: {
            model: 'answers',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'set null'
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.removeColumn('questions', 'trusted_answer_id');
  }
};
