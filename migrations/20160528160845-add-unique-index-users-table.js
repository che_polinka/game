'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addIndex('users',
      ['email', 'phone'],
      {indicesType: 'UNIQUE'}
    );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeIndex('users', ['email', 'phone']);
  }
};
