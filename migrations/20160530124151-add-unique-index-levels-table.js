'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addIndex('levels', ['name'], { indicesType: 'UNIQUE' });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeIndex('levels', ['name']);
  }
};
