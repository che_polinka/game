'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addIndex('users', ['rating', 'id']);
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeIndex('users', ['rating', 'id']);
  }
};
