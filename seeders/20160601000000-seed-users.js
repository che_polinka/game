'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    function randomInteger(min, max) {
      var rand = min + Math.random() * (max + 1 - min);
      rand = Math.floor(rand);
      return rand;
    }

    var
      i = 0
    , users = []
    , names = 'Петр Анастасия Виктор Павел Константин Илья Марат Винни-Пух Марина Лунатик Алина Юрий Вероника'.split(' ')
    ;
    for ( ; i < names.length; ++i) {
      users.push({
        id: i + 1,
        name: names[ i ],
        email: 'example_'+i+'@example.com',
        phone: '+79812345678',
        address: 'Pass',
        created_at: new Date(),
        updated_at: new Date(),
      });
    }

    return queryInterface.bulkInsert('users', users, {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('users', null, {});
  }
};
