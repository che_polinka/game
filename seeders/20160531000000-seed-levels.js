'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var levels = [
      {
        'id': 1,
        'name': 'easy',
        'rate': 1,
      },
      {
        'id': 2,
        'name': 'hard',
        'rate': 5,
      },
    ];

    return queryInterface.bulkInsert('levels', levels, {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('levels', null, {});
  }
};
