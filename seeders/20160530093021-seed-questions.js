'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var data = [
      { id: 1,
        text: 'Как называется одна из самых эффективных методик лечения заболеваний позвоночника и восстановления после различных травм?',
        tip: 'В <a href="https://www.dikul.org/search" target="_blank">поисковой строке</a> написав слово: методика, вы найдете ответ на вопрос'
      },
      { id: 2,
        text: 'Это прогрессирующая деформирующая дорсопатия, возникающая в период роста организма. Как правило, возникает в подростковом возрасте, когда происходит рост костей и тканей. Возникает в 1% случаев в одинаковой степени, как у мальчиков, так и у девочек?',
        tip: 'Ответ на странице <a href="https://www.dikul.org/chastye-zabolevanija/" target="_blank">Частых заболеваний</a>'
      },
      { id: 3,
        text: 'Как называют уменьшение и сужение канала, находящегося в области шеи?',
        tip: 'Правильный ответ в <a href="https://www.dikul.org/handbooks/encyclopedia/?alph=%D0%A1" target="_blank">Энциклопедии сайта</a>'
      },
      { id: 4,
        text: 'Что определяет аппарат биоимпедансометрии “Медасс”?',
        tip: 'Об аппарате «Медасс» читайте <a href="https://www.dikul.org/equipment/apparat-bioimpedansometrii-medass" target="_blank">здесь</a>'
      },
      { id: 5,
        text: 'Червь, распространенный в тех регионах, где активно развито собаководство?',
        tip: 'Верный <a href="https://www.dikul.org/handbooks/encyclopedia/?alph=%D0%AD" target="_blank">ответ</a>'
      },
      { id: 6,
        text: 'Из скольки сегментов состоит шейный отдел спинного мозга?',
        tip: 'Вам поможет <a href="https://www.dikul.org/search" target="_blank">поиск по сайту</a> по запросу: шейный отдел спинного мозга'
      },
      { id: 7,
        text: 'Как называется инфекционное поражение мочевого пузыря?',
        tip: 'Верный ответ на <a href="https://www.dikul.org/handbooks/encyclopedia/?PAGEN_1=6" target="_blank">этой странице</a>'
      },
      { id: 8,
        text: 'Как называется воспаление вены с образованием тромба?',
        tip: 'Данное заболевание является противопоказанием в использовании этого аппарат, ответ на <a href="https://www.dikul.org/equipment/ionoson-lechenie-zashchemleniya-sedalishchnogo-nerva" target="_blank">этой странице</a>'
      },
      { id: 9,
        text: 'Как можно назвать нарушение целостности (повреждение) органов и тканей организма за счет воздействия факторов внешней среды?',
        tip: 'По <a href="https://www.dikul.org/search" target="_blank">запросу</a>: воздействия внешней среды вы найдете верный ответ'
      },
      { id: 10,
        text: 'Как «в народе» называется болезнь Нотта?',
        tip: 'Вам поможет <a href="https://www.dikul.org/search" target="_blank">поиск по сайту</a> по слову Нотта'
      },
      { id: 11,
        text: 'Как еще называется заболевание адгезивный капсулит?',
        tip: 'Воспользовавшись <a href="https://www.dikul.org/search" target="_blank">поиском по сайту</a>, вы увидите ответ'
      },
      { id: 12,
        text: 'Как называется область медицины, которая занимается диагностикой, лечением и профилактикой заболеваний мочеполовой сферы?',
        tip: 'Чтобы правильно ответить, перейдите в <a href="https://www.dikul.org/treatment/" target="_blank">раздел Лечение</a> в анонсе страницы найдете определение данной области медицины'
      },
      { id: 13,
        text: 'Узкоспециализированный раздел медицины, занимающийся диагностикой и лечением заболеваний сердечнососудистой системы?',
        tip: 'Верный ответ находится на этой <a href="https://www.dikul.org/treatment/" target="_blank">странице</a>'
      },
      { id: 14,
        text: 'Раздел медицины, занимающийся вопросами профилактики и лечения заболеваний женской репродуктивной сферы?',
        tip: 'Ответ на <a href="https://www.dikul.org/treatment/" target="_blank">странице Лечения</a>'
      },
      { id: 15,
        text: 'Стойкое искривление позвоночника?',
        tip: '<a href="https://www.dikul.org/search" target="_blank">Поиск по сайту</a> поможет найти точный ответ'
      },
      { id: 16,
        text: 'Сколько степеней сколиоза различают по В. Д. Чаклину?',
        tip: 'Верный ответ в Диагностике на этой <a href="https://www.dikul.org/simptomy/narushenie-osanki/" target="_blank">странице</a>'
      },
      { id: 17,
        text: 'Название этого заболевания восходит к латинскому слову — «hemigranea», английскому – «megrim» и буквальному переводу с французского языка — «migraine»?',
        tip: 'Верный ответ <a href="https://www.dikul.org/handbooks/article/migren-prichiny-simptomy-vidy" target="_blank">здесь</a>'
      },
      { id: 18,
        text: 'Как называется лечебное воздействие на органы и ткани организма холодовых факторов, которые снижают температуру тканей до пределов их устойчивости?',
        tip: 'Ответ на <a href="https://www.dikul.org/equipment/apparat-kriodzhet-s200/" target="_blank">странице с оборудованием</a>, который используется в этом методе лечения'
      },
      { id: 19,
        text: 'Название этого метода лечения происходит от греч. «physis» и «therapeia» – лечение природой?',
        tip: 'Этот метод указан на странице <a href="https://www.dikul.org/treatment/" target="_blank">лечения</a>'
      },
      { id: 20,
        text: 'Кокцигодония - это?',
        tip: 'Чтобы узнать, что это перейдите по <a href="https://www.dikul.org/simptomy/yagodicy/boli-v-kopchike" target="_blank">ссылке</a>'
      },
      { id: 21,
        text: 'Непроизвольная дрожь и подергивание кистей в результате частого сокращения мышц называется?',
        tip: 'Чтобы верно ответить на вопрос, перейдите на <a href="https://www.dikul.org/handbooks/encyclopedia/lekarstvenno-indutsirovannyy-tremor" target="_blank">страницу с описанием</a>'
      },
      { id: 22,
        text: 'наука, раздел клинической медицины, изучающий нарушение функций связочной и костно-мышечной системы, их профилактику, диагностику и лечение?',
        tip: 'Правильный ответ <a href="https://www.dikul.org/treatment/ortopedija/" target="_blank">здесь</a>'
      },
      { id: 23,
        text: 'какой врач поможет избавиться от болей в позвоночнике?',
        tip: 'Верный ответ на этой <a href="https://www.dikul.org/anxietys/boli-v-spine/" target="_blank">странице</a>'
      },
      { id: 25,
        text: 'Как называется разрастание костной ткани?',
        tip: 'Ответ найдете <a href="https://www.dikul.org/bolezni/shpora-pjatochnaja" target="_blank">здесь</a>'
      },
      { id: 27,
        text: 'Сколько в среднем необходимо пройти процедур для лечения межпозвоночной грыжи лазером?',
        tip: 'Для того, чтобы верно ответить на вопрос нужно перейти на <a href="https://www.dikul.org/treatment/hilt-terapiya-lechenie-gryzh-lazerom/" target="_blank">страницу о лечении</a>'
      },
      { id: 28,
        text: 'Для устранения каких недугов из перечисленных в списке, активно применяется ХИЛТ терапия?',
        tip: 'На <a href="https://www.dikul.org/equipment/hilt-terapiya-lechenie-gryzh-lazerom" target="_blank">этой странице</a> найдете ответ на вопрос '
      },
      { id: 29,
        text: 'Что послужило мощным толчком к разработке собственной системы лечения и восстановления после травм опорно-двигательного аппарата для доктора Дикуля?',
        tip: 'Об этом читайте на <a href="https://www.dikul.org/about/biography/" target="_blank">этой странице</a>'
      },
      { id: 30,
        text: 'В каком году появился первый медицинский центр В.И. Дикуля?',
        tip: 'Ответ найдете на странице <a href="https://www.dikul.org/about/biography/" target="_blank">биографии В.И. Дикуля</a>'
      },
    ];

    return queryInterface.bulkInsert('questions', data, {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('questions', null, {});
  }
};
