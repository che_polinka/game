'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var data = [
      { text: 'Методика Дикуля',
        question_id: 1 },
      { text: 'Методика Микуля',
        question_id: 1 },
      { text: 'Методика Викуля',
        question_id: 1 },

      { text: 'Сколиоз',
        question_id: 2 },
      { text: 'Кифоз Шейермана ',
        question_id: 2 },
      { text: 'Грыжа межпозвоночного диска',
        question_id: 2 },

      { text: 'Стеноз шейных позвонков',
        question_id: 3 },
      { text: 'Кифоз Шейермана ',
        question_id: 3 },
      { text: 'Ватный” позвоночник',
        question_id: 3 },

      { text: 'Состав тканей',
        question_id: 4 },
      { text: 'Частоту сердечных сокращений',
        question_id: 4 },
      { text: 'Уровень глюкозы в крови',
        question_id: 4 },

      { text: 'Эхинококк',
        question_id: 5 },
      { text: 'Земляной червь',
        question_id: 5 },
      { text: 'Волосатик',
        question_id: 5 },

      { text: 'из 7',
        question_id: 6 },
      { text: 'из 15',
        question_id: 6 },
      { text: 'из 21',
        question_id: 6 },

      { text: 'Цистит',
        question_id: 7 },
      { text: 'Холестит',
        question_id: 7 },
      { text: 'Авитоминоз',
        question_id: 7 },

      { text: 'Тромбофлебит',
        question_id: 8 },
      { text: 'Тромбоз',
        question_id: 8 },
      { text: 'Закупорка',
        question_id: 8 },

      { text: 'Смерть',
        question_id: 9 },
      { text: 'Травма',
        question_id: 9 },
      { text: 'Порез',
        question_id: 9 },

      { text: 'Синдром правой руки',
        question_id: 10 },
      { text: 'Синдром щелкающего пальца',
        question_id: 10 },
      { text: 'Синдром отсутствующего уха',
        question_id: 10 },

      { text: 'Лишай',
        question_id: 11 },
      { text: 'Синдром “дрожащего века”',
        question_id: 11 },
      { text: 'Синдром «замороженного плеча»',
        question_id: 11 },

      { text: 'Урология',
        question_id: 12 },
      { text: 'Косметология',
        question_id: 12 },
      { text: 'Гинекология',
        question_id: 12 },

      { text: 'Сердцелогия',
        question_id: 13 },
      { text: 'Анатомия',
        question_id: 13 },
      { text: 'Кардиология',
        question_id: 13 },

      { text: 'Урология ',
        question_id: 14 },
      { text: 'Гинекология',
        question_id: 14 },
      { text: 'Маммология ',
        question_id: 14 },

      { text: 'Сколиоз',
        question_id: 15 },
      { text: 'Радикулит',
        question_id: 15 },
      { text: 'Хандроз',
        question_id: 15 },

      { text: '6',
        question_id: 16 },
      { text: '4',
        question_id: 16 },
      { text: '12',
        question_id: 16 },

      { text: 'Малярия',
        question_id: 17 },
      { text: 'Мигрень',
        question_id: 17 },
      { text: 'Хандра',
        question_id: 17 },

      { text: 'Электрофорез',
        question_id: 18 },
      { text: 'Криотерапия',
        question_id: 18 },
      { text: 'Магнитотерапия',
        question_id: 18 },

      { text: 'Магнитотерапия',
        question_id: 19 },
      { text: 'Физиотерапия',
        question_id: 19 },
      { text: 'Хирургия',
        question_id: 19 },

      { text: 'Когда боль концентрируется непосредственно в области пупка',
        question_id: 20 },
      { text: 'Когда боль концентрируется непосредственно в области копчика',
        question_id: 20 },
      { text: 'Когда боль концентрируется непосредственно в области затылка',
        question_id: 20 },

      { text: 'Тремор',
        question_id: 21 },
      { text: 'Психоз',
        question_id: 21 },
      { text: 'Доплерография',
        question_id: 21 },

      { text: 'Биология',
        question_id: 22 },
      { text: 'Ортопедия',
        question_id: 22 },
      { text: 'Хирургия',
        question_id: 22 },

      { text: 'Невролог',
        question_id: 23 },
      { text: 'Невропатолог',
        question_id: 23 },
      { text: 'Терапевт',
        question_id: 23 },

      { text: 'Отпрыск',
        question_id: 25 },
      { text: 'Остеофит',
        question_id: 25 },
      { text: 'Пятка',
        question_id: 25 },

      { text: 'От 1 до 2',
        question_id: 27 },
      { text: 'От 5 до 10',
        question_id: 27 },
      { text: 'От 15 до 20',
        question_id: 27 },

      { text: 'Нарушение функций зрения',
        question_id: 28 },
      { text: 'Кожные заболевания',
        question_id: 28 },
      { text: 'Заболевания позвоночника',
        question_id: 28 },

      { text: 'Собственная травма - паралич ног после падения с большой высоты;',
        question_id: 29 },
      { text: 'Поспорил с преподавателем ВУЗа;',
        question_id: 29 },
      { text: 'Доктор  профессионально занимался спортивной гимнастикой в детстве и после первой травмы решил посвятить себя этому делу',
        question_id: 29 },

      { text: 'В 1958',
        question_id: 30 },
      { text: 'В 2011',
        question_id: 30 },
      { text: 'В 1988',
        question_id: 30 },
    ];

    data.forEach(function(row, i) {
      data[i].id = i + 1
    });

    return queryInterface.bulkInsert('answers', data, {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('answers', null, {});
  }
};
