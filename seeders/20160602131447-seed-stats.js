'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      function randomInteger(min, max) {
        var rand = min + Math.random() * (max + 1 - min);
        rand = Math.floor(rand);
        return rand;
      }

      var i = 0, stats = [];
      for ( ; i < 30; ) {
        stats.push({
          id: ++i,
          user_id: randomInteger(1, 13),
          level_id: randomInteger(1, 2),
          num_of_answers: randomInteger(6, 20),
          total_time: randomInteger(500, 1800),
          created_at: new Date(),
          updated_at: new Date(),
        });
      }

      return queryInterface.bulkInsert('stats', stats, {});
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.bulkDelete('stats', null, {});
  }
};
