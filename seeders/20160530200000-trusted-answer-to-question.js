'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var map = [
      {id:  1, trusted_answer_id:  1},
      {id:  2, trusted_answer_id:  5},
      {id:  3, trusted_answer_id:  7},
      {id:  4, trusted_answer_id: 10},
      {id:  5, trusted_answer_id: 13},
      {id:  6, trusted_answer_id: 16},
      {id:  7, trusted_answer_id: 19},
      {id:  8, trusted_answer_id: 22},
      {id:  9, trusted_answer_id: 26},
      {id: 10, trusted_answer_id: 29},

      {id: 11, trusted_answer_id: 33},
      {id: 12, trusted_answer_id: 34},
      {id: 13, trusted_answer_id: 39},
      {id: 14, trusted_answer_id: 41},
      {id: 15, trusted_answer_id: 43},
      {id: 16, trusted_answer_id: 47},
      {id: 17, trusted_answer_id: 50},
      {id: 18, trusted_answer_id: 53},
      {id: 19, trusted_answer_id: 56},
      {id: 20, trusted_answer_id: 59},

      {id: 21, trusted_answer_id: 61},
      {id: 22, trusted_answer_id: 65},
      {id: 23, trusted_answer_id: 67},
      {id: 25, trusted_answer_id: 71},
      {id: 27, trusted_answer_id: 74},
      {id: 28, trusted_answer_id: 78},
      {id: 29, trusted_answer_id: 79},
      {id: 30, trusted_answer_id: 84},
    ];

    var
      id
    , trusted_answer_id
    , result;

    map.forEach(function(row, i) {
      id = row.id;
      trusted_answer_id = row.trusted_answer_id;

      result = queryInterface.bulkUpdate('questions', {trusted_answer_id: trusted_answer_id}, {id: id}, {}, {});
    });

    return result;
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkUpdate('questions', {trusted_answer_id: null}, null, {}, {});
  }
};
