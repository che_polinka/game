# Intro

## Package list

- [dotenv](https://www.npmjs.com/package/dotenv)
- [csurf]()
- [sequelize]()
- [sequelize-cli]()
- [shuffle-array](https://www.npmjs.com/package/shuffle-array)
- [socket.io]()

## Build

### Build image
    docker build -t app .

### Run container
    docker run -it --rm \
    -p 8080:3000 \
    -v /usr/src/app/node_modules \
    -v $(pwd):/usr/src/app \
    app

## Use Compose

### Create and start containers
    docker-compose up

### Auto restarting

Into docker container app running as `nodemon` process. Change your code and reload browser page.

## Use Compose with Nginx

### Create and start containers
    docker-compose -f docker-compose-nginx.yml up

### Destroy containers
    docker-compose -f docker-compose-nginx.yml down -v


## Install dependencies

### Bower

    docker-compose exec app \
    npm run-script bower:i

## Sequelize

>Sequelize is a promise-based ORM for Node.js and io.js. It supports the dialects PostgreSQL, MySQL, SQLite and MSSQL and features solid transaction support, relations, read replication and more.

### Sequelize help

    docker-compose exec app sequelize

### Create new model and migrate

    docker-compose exec app \
    sequelize model:create \
    --underscored \
    --name user \
    --attributes email:string,phone:string,name:string,address:string

### Up migrate

    docker-compose exec app \
    sequelize db:migrate

### Create new migrate

    docker-compose exec app \
    sequelize migration:create \
    --underscored \
    --name add-index-user

### Seed

    docker-compose exec app \
    sequelize db:seed --seed <filename.js>

    docker-compose exec app \
    sequelize db:seed:all
