'use strict';

var gulp = require('gulp');

var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var notify = require("gulp-notify");
var uglify = require('gulp-uglify');
var multipipe = require('multipipe');

var config = {
    publicPath: 'public',
    scriptsPath: 'src/js/'
};

var paths = {
    stylus: ['src/stylus/**/*.styl'],
    scripts: ['src/js/*.js'],
    animationScripts: ['src/js/animation/*.js']
};

gulp.task('stylus', function() {
    return gulp.src(paths.stylus)
        .pipe(stylus())
        .pipe(gulp.dest("public/css"));
});

gulp.task('scripts', function() {
    return multipipe(
        gulp.src([
            config.scriptsPath + 'new.js',
            config.scriptsPath + 'auth.js'
        ]),
        uglify(),
        concat('main.js'),
        gulp.dest(config.publicPath + '/javascripts')
    ).on('error', notify.onError());
});

gulp.task('animation', function() {
    return multipipe(
        gulp.src(paths.animationScripts),
        uglify(),
        gulp.dest(config.publicPath + '/javascripts/animation')
    ).on('error', notify.onError());
});

// Run all task
gulp.task('build', ['stylus', 'scripts', 'animation']);

// Default task for Gulp
gulp.task('default', ['build']);

gulp.task('watch', function() {
    gulp.watch(paths.stylus,  ['stylus']);
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.animationScripts, ['animation']);
});
