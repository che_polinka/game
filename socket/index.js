var socket = require('socket.io');
var models = require('../models');

module.exports = function(server) {
  var io = socket(server);

  io.on('connection', onConnection);

  function onConnection(socket) {
    // socket.emit('broadcast:stats', output);

    /**
     * Проверить соответствие ответа к вопросу.
     *
     * @return {bool}
     */
    socket.on('request:correct answer?', function(data) {
      var
        answerId = parseInt(data.answerId, 10)
      , questionId = parseInt(data.questionId, 10)

      , output = { error: null, result: null, questionId: questionId, answerId: answerId }
      ;

      if (isNaN(answerId) || isNaN(questionId)) {
        output.error = 'Bad data';
        socket.emit('response:correct answer?', output);

        return;
      }

      models.question.findById(questionId)
        .then(function(question) {
          question.getTrustedAnswer().then(function(answer) {
            output.result = answer.id === answerId ? true : false;
            socket.emit('response:correct answer?', output);
          })
        })
        .catch(function(err) {
          output.error = err.message;
          socket.emit('response:correct answer?', output);
        })

    })
  }
}
